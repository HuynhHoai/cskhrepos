/**
 * @format
 */
import React, { useEffect, useState } from 'react';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './source/reducers/rootReducer';
import rootSaga from './source/redux/rootSagas';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { fcmService } from './source/helpers/FCMService';
import { localNotificationService } from './source/helpers/LocalNotificationService';

//Test screen
import DashBoard from './source/screens/DashBoard';
import SelectPage from './source/screens/SelectPage';
import HomeNavigation from './source/navigation/HomeNavigation';
import Toast from 'react-native-toast-message';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

const App = () => {

    useEffect(() => {
        fcmService.registerAppWithFCM()
        fcmService.register(onRegister, onNotification, onOpenNotification)
        localNotificationService.createChannel()
        localNotificationService.configure(onOpenNotification)
    
        function onRegister(token) {
          console.log("[App] onRegister: ", token)
        }
    
        function onNotification(notify) {
            console.log("[App] onNotification: ", notify)
            const options = {
                soundName: 'default',
                playSound: true
            }
            localNotificationService.showNotification(
                0,
                notify.title,
                notify.body,
                notify,
                options
            )
        }
    
        function onOpenNotification(notify) {
            console.log("[App] onOpenNotification: ", notify)
            alert("Open Notification: " + notify.body)
        }
    
        return () => {
            console.log("[App] unRegister")
            fcmService.unRegister()
            localNotificationService.unregister()
        }
    
      }, [])

    return (
        <Provider store={store}>
            <NavigationContainer>
                <HomeNavigation/>
            </NavigationContainer>
            <Toast ref={(ref) => Toast.setRef(ref)} />
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => App);
