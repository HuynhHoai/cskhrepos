/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React from 'react';
 import {
   Text
 } from 'react-native';
 
 const App = () => {
   return (
     <Text>Cham Soc Khach Hang</Text>
   );
 };
 
 export default App;
 