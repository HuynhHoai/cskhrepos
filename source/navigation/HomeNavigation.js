import React, { useState, useEffect } from 'react';

import HomeScreen from '../screens/HomeScreen';
import MessageScreen from '../screens/MessageScreen';
import SelectPage from '../screens/SelectPage';
import Cart from '../screens/Cart';
import Notification from '../screens/Notification';
import CreateProduct from '../screens/CreateProduct';
import AppNavigation from './AppNavigation';
import BottomNavigation from './BottomNavigation';
import TotalOrders from '../screens/TotalOrders';
import OrderDetail from '../screens/OrderDetail';
import DetailProduct from '../screens/DetailProduct';
import EditDetailProduct from '../screens/EditDetailProduct';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { useSelector } from 'react-redux';
//Splash screen
import SplashScreen from 'react-native-splash-screen';
import LoginScreen from '../screens/Login';
import Register from '../screens/Register';
const Stack = createStackNavigator();

export default function HomeNavigation() {

    const [isLoad, setLoad] = useState(false)
    const [authenticated, setAuthenticated] = useState(false)
    const userState = useSelector(state => state.userReducers != null ? state.userReducers : null)
    useEffect(()=> {
        setLoad(true)
        async function handleGetData() {
            await getData()
        }
        handleGetData()
        setTimeout(function(){
                SplashScreen.hide();
            }
            , 1000);
        return () => {
            null
        }
    },[]);

    const getData = async () => {
        try {
            const access_token = await AsyncStorage.getItem('@access_token')
            const user_info = await AsyncStorage.getItem('@user_info')
            if(access_token !== null && user_info !== null) {
                setAuthenticated(true)
            }
        } catch(e) {
            // error reading value
        }
    }
        
    if (isLoad && userState != null) {
        setLoad(false)
        setAuthenticated(true)
    }

    return (
        authenticated ? (
            <>
                <Stack.Navigator
                    initialRouteName="Home"
                    screenOptions={{
                        headerShown: false,
                        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                    }}
                >   
                    <Stack.Screen name='Home' component={BottomNavigation}/>
                    <Stack.Screen name='AppScreen' component={AppNavigation}/>
                    <Stack.Screen name='Cart' component={Cart}/>
                    <Stack.Screen name='CreateProduct' component={CreateProduct}/>
                    <Stack.Screen name='TotalOrders' component={TotalOrders}/>
                    <Stack.Screen name='OrderDetail' component={OrderDetail}/>
                    <Stack.Screen name='Notification' component={Notification}/>
                    <Stack.Screen name='DetailProduct' component={DetailProduct}/>
                    <Stack.Screen name='EditDetailProduct' component={EditDetailProduct}/>
                </Stack.Navigator>
            </>
        ) : (
            <>
                <Stack.Navigator
                    screenOptions={{
                        headerShown: false,
                        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
                    }}
                >   
                    <Stack.Screen name='LoginScreen' component={LoginScreen}/>
                    <Stack.Screen name='Register' component={Register}/>
                </Stack.Navigator>
            </>
        )
    )
}