import React from 'react';

import HomeNavigation from './HomeNavigation';
import Products from '../screens/Products';
import Notification from '../screens/Notification';
import DashBoard from '../screens/DashBoard';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { StyleSheet, Dimensions, View, TouchableOpacity, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
const Tab = createBottomTabNavigator();

const styles = StyleSheet.create({
    tabContainer: { 
        flexDirection: 'row',
        alignSelf: "center",
        position: "absolute",
        bottom: 0,
        backgroundColor: colors.white,
        borderTopWidth: 1,
        borderTopColor: colors.powder_gray,
        justifyContent: "space-between",
        width: windowWidth,
        paddingHorizontal: 15,
    },
    button: {
        width: windowWidth*0.3, alignItems: "center",
    },
    line: {
        borderTopColor: colors.white, borderTopWidth: 2, paddingTop: 10, width: 35, alignItems: "center"
    },
    text: {
        fontFamily: fonts.UTM_Avo,
        fontSize: 12
    }
})

function MyTabBar({ state, descriptors, navigation }) {
    return (
        <View style={styles.tabContainer}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label =
                    options.tabBarLabel !== undefined
                    ? options.tabBarLabel
                    : options.title !== undefined
                    ? options.title
                    : route.name;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                    type: 'tabPress',
                    target: route.key,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                    navigation.navigate(route.name);
                    }
                };

                const onLongPress = () => {
                    navigation.emit({
                    type: 'tabLongPress',
                    target: route.key,
                    });
                };
                
                switch (index) {
                    case 0:
                        return (
                            <TouchableOpacity
                                key={index}
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={onPress}
                                onLongPress={onLongPress}
                                style={styles.button}
                            >
                            <View style={[styles.line, isFocused && {borderTopColor: colors.purple}]}>
                                <Icon name="home" size={20} color={isFocused ? colors.purple : colors.powder_gray} />
                            </View>
                            <Text style={[styles.text, {color: isFocused ? colors.purple : colors.black}]}>Home</Text>
                            </TouchableOpacity>
                        );
                    case 1:
                        return (
                            <TouchableOpacity
                                key={index}
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={onPress}
                                onLongPress={onLongPress}
                                style={styles.button}
                            >
                                <View style={[styles.line, isFocused && {borderTopColor: colors.purple}]}>
                                    <Icon name="cubes" size={20} color={isFocused ? colors.purple : colors.powder_gray} />
                                </View>
                                <Text style={[styles.text, {color: isFocused ? colors.purple : colors.black}]}>Product</Text>
                            </TouchableOpacity>
                        );
                    default:
                        return (
                            <TouchableOpacity
                                key={index}
                                accessibilityRole="button"
                                accessibilityStates={isFocused ? ['selected'] : []}
                                accessibilityLabel={options.tabBarAccessibilityLabel}
                                testID={options.tabBarTestID}
                                onPress={onPress}
                                onLongPress={onLongPress}
                                style={styles.button}
                            >
                                <View style={[styles.line, isFocused && {borderTopColor: colors.purple}]}>
                                    <Icon name="bell" size={20} color={isFocused ? colors.purple : colors.powder_gray} />
                                </View>
                                <Text style={[styles.text, {color: isFocused ? colors.purple : colors.black}]}>Notification</Text>
                            </TouchableOpacity>
                        );
                }
            })}
        </View>
    );
}

export default function BottomNavigation() {
    return (
        <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>   
            <Tab.Screen name='DashBoard' component={DashBoard}/>
            <Tab.Screen name='Products' component={Products}/>
            <Tab.Screen name='Notification' component={Notification}/>
        </Tab.Navigator>
    )
}