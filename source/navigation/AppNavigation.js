import React from 'react';

import HomeScreen from '../screens/HomeScreen';
import MessageScreen from '../screens/MessageScreen';
import DashBoard from '../screens/DashBoard';
import SelectPage from '../screens/SelectPage';
import Cart from '../screens/Cart';
import Notification from '../screens/Notification';
import CreateProduct from '../screens/CreateProduct';
import TotalOrders from '../screens/TotalOrders';
import OrderDetail from '../screens/OrderDetail';

import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function AppNavigation() {
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS
            }}
        >   
            <Stack.Screen name='SelectPage' component={SelectPage}/>
            <Stack.Screen name='TotalOrders' component={TotalOrders}/>
            <Stack.Screen name='OrderDetail' component={OrderDetail}/>
            <Stack.Screen name='HomeScreen' component={HomeScreen}/>
            <Stack.Screen name='MessageScreen' component={MessageScreen}/>
            <Stack.Screen name='Cart' component={Cart}/>
        </Stack.Navigator>
    )
}