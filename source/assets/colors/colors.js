export const colors = {
    black:"#000000",
    white: "#fff",
    gray: "#EBEBEB",
    light_gray: "#F4F3F3",
    orange: "#f4a43d",
    strong_orange: "#EF7212",
    yellow: "#F2C94E",
    powder_gray: "#c6cad4",
    green: "#6DAB3C",
    red: "#E43C06",
    purple: "#8774F3",
    light_purple: "#BBC3F3",
    dark_gray:"#808080"
}