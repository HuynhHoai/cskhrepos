import {Platform} from 'react-native';
export const fonts = {
    UTM_Avo: Platform.OS === "ios" ? "UTM-Avo" : "UTM Avo",
    UTM_AvoBold: Platform.OS === "ios" ? "UTMAvoBold" : "UTM AvoBold",
}