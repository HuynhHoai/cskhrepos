import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';

import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginTop: 10,
        marginHorizontal: 5,
        padding: 10,
        borderRadius: 10,
        alignItems: "center",
    },
    appImage: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    text: {
        fontFamily: fonts.UTM_AvoBold,
        fontSize: 12
    }
})

const AppComponent = ({navigation, item}) => {

    const handlePress = () => {
        navigation.navigate('AppScreen')
    }

    return (
        <Pressable onPress={handlePress} style={styles.container}>
            <Image style={styles.appImage} source={item.id == 1 ? require('../assets/icons/facebook.png') : require('../assets/icons/zalo.png')} />
            <Text style={styles.text}>{item.name}</Text>
        </Pressable>
    )
}

export default AppComponent;