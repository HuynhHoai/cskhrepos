import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';

import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginHorizontal: 5,
        width: windowWidth*0.9,
        padding: 10,
        borderRadius: 10,
        flexDirection: "row",
        borderBottomColor: colors.powder_gray,
        borderBottomWidth: 0.5,
        justifyContent: "space-between",
        alignItems: "center"
    },
    appImage: {
        height: 40, 
        width: 40,
        borderRadius: 40/5
    },
    button: {
        backgroundColor: colors.purple,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 5,
        paddingVertical: 5
    },
    subTitle: {
        fontSize: 16,
        fontFamily: fonts.UTM_AvoBold
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
})

const PageComponent = ({navigation, item, handleCheckPage}) => {

    return (
        <Pressable onPress={handleCheckPage} style={styles.container}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <Image style={styles.appImage} source={require('../assets/images/icon.png')} />
                <View>
                    <Text style={styles.subTitle}>{item.name}</Text>
                    <View>
                        <Text style={styles.text}>{item.category}</Text>
                    </View>
                </View>
            </View>
            <Pressable onPress={handleCheckPage}>
                    <Icon name="check-circle" size={20} color={item.selected == true ? colors.purple : colors.light_purple} />
            </Pressable>
        </Pressable>
    )
}

export default PageComponent;