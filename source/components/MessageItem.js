import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';

import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginTop: 10,
        marginHorizontal: 5,
        padding: 10,
        width: windowWidth*0.9,
        borderRadius: 10,
        flexDirection: "row",
        borderBottomColor: colors.powder_gray,
        borderBottomWidth: 0.5,
        justifyContent: "space-between"
    },
    userIcon: {
        height: 40, 
        width: 40,
        borderRadius: 40/5
    },
    title: {
        fontSize: 14,
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo,
        fontSize: 13
    },
    tinyText: {
        fontSize: 12,
        fontFamily: fonts.UTM_Avo,
    },
    line: {
        flexDirection :"row", justifyContent: "space-between", alignItems: "center"
    }
})

const MessageItem = ({navigation, item}) => {

    const openMessage = (item) => {
        navigation.navigate('MessageScreen', {item: item});
    }

    return (
        <Pressable onPress={() => openMessage(item)} style={styles.container}>
            <View style={styles.line}>
                <Image style={styles.userIcon} source={require('../assets/images/icon.png')} />
                <View style={{paddingLeft: 10, maxWidth: windowWidth*0.5}}>
                    <Text numberOfLines={1} style={styles.title}>{item.name}</Text>
                    <Text numberOfLines={1} style={styles.text}>{item.body}</Text>
                </View>
            </View>
            <Text style={styles.tinyText}>just now</Text>
        </Pressable>
    )
}

export default MessageItem;