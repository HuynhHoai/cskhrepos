import React, { useState } from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';

const styles = StyleSheet.create({
    inputSection: {
        marginTop: 10,
        borderRadius: 15,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: colors.powder_gray,
        borderWidth: 1,
    },
    input:{
        flex: 1,
        borderRadius: 15,
        fontFamily: fonts.UTM_Avo,
    },
    inputIcon: {
        padding: 10
    }
})

export default function Input({iconName, iconSize, iconColor, placeholder, value, multiline, keyboardType, height, onChangeText}) {
    const [isFocus, setFocus] = useState(false)
    return (
        <View style={[styles.inputSection, {borderColor: isFocus ? colors.purple : colors.powder_gray, height}]}>
            <Icon style={styles.inputIcon} name={iconName} size={iconSize} color={iconColor} />
            <TextInput
                onFocus={() => setFocus(true)}
                onBlur={() => setFocus(false)}
                multiline={multiline}
                keyboardType={keyboardType}
                style={styles.input}
                placeholder={placeholder}
                underlineColorAndroid="transparent"
                value={value}
                onChangeText={onChangeText}
            />
        </View>
    )
}