import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colors } from '../assets/colors/colors';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    container: {
        padding: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: colors.powder_gray,
        marginTop: 10
    },
    line: {
        flexDirection: "row",
        justifyContent: "space-between"
    }
})

export default function OrderItem({item}) {
    console.log(item);
    return (
        <View style={styles.container}>
            <View style={styles.line}>
                <View style={{flexDirection: "row"}}>
                    <Icon name="hashtag" size={20} color={colors.powder_gray} />
                    <Text style={{marginLeft: 5}}>{item.id}</Text>
                </View>
                <Text>Moi</Text>
            </View>

            <View style={styles.line}>
                <View style={{flexDirection: "row"}}>
                    <Icon name="user" size={20} color={colors.powder_gray} />
                    <Text style={{marginLeft: 5}}>Tao boi:</Text>
                </View>
                <Text>{item.createBy}</Text>
            </View>

            <View style={styles.line}>
                <View style={{flexDirection: "row"}}>
                    <Icon name="bitcoin" size={20} color={colors.powder_gray} />
                    <Text style={{marginLeft: 5}}>Thu ho:</Text>
                </View>
                <Text>{item.price} VND</Text>
            </View>
            <View style={styles.line}>
                <View style={{flexDirection: "row"}}>
                    <Icon name="map" size={20} color={colors.powder_gray} />
                    <Text style={{marginLeft: 5}}>Dia chi:</Text>
                </View>
                <Text>{item.address}</Text>
            </View>
            <View style={styles.line}>
                <View style={{flexDirection: "row"}}>
                    <Icon name="phone" size={20} color={colors.powder_gray} />
                    <Text style={{marginLeft: 5}}>So DT:</Text>
                </View>
                <Text>{item.phone}</Text>
            </View>
            <View style={styles.line}>
                <View style={{flexDirection: "row"}}>
                    <Icon name="calendar" size={20} color={colors.powder_gray} />
                    <Text style={{marginLeft: 5}}>Tao luc:</Text>
                </View>
                <Text>{item.createAt}</Text>
            </View>
        </View>
    )
}