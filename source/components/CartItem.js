import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';

import { colors } from '../assets/colors/colors';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginTop: 10,
        marginHorizontal: 5,
        padding: 10,
        width: windowWidth*0.9,
        borderRadius: 10,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "space-between"
    },
    appImage: {
        height: 80, 
        width: 80,
        borderRadius: 80/5
    },
    title: {
        fontSize: 16,
        fontWeight: "bold"
    },
    content: {
        fontWeight: "bold"
    }
})

const CartItem = ({navigation, item}) => {
    const handlePress = () => {
        console.log("Vi Khanh Tun")
        // navigation.navigate('SelectPage')
    }

    return (
        <Pressable onPress={handlePress} style={styles.container}>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <Image style={styles.appImage} source={require('../assets/images/icon.png')} />
                <View>
                    <Text style={styles.title}>{item.name}</Text>
                    <Text>Huynh Van Hoai Dep Trai</Text>
                    <Text style={styles.content}>{item.number} x {item.price} VND</Text>
                </View>
            </View>
            <Pressable>
                <Icon name="trash" size={25} color={colors.red} />
            </Pressable>
        </Pressable>
    )
}

export default CartItem;