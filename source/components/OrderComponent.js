import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginHorizontal: 5,
        width: windowWidth*0.9,
        padding: 10,
        borderRadius: 10,
        flexDirection: "row",
        borderBottomColor: colors.powder_gray,
        borderBottomWidth: 0.5,
        justifyContent: "space-between",
        alignItems: "center"
    },
    appImage: {
        height: 40, 
        width: 40,
        borderRadius: 40/5,
        backgroundColor: colors.light_purple,
        marginRight: 10
    },
    title: {
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo,
        fontSize: 12
    },
    tinyText: {
        fontSize: 12,
        fontFamily: fonts.UTM_Avo,
    },
    line: {
        flexDirection :"row", alignItems: "center", justifyContent: "space-between"
    }
})

const OrderComponent = ({navigation, item}) => {
    const handlePress = () => {
        console.log(navigation);
        navigation.navigate("OrderDetail");
    }

    return (
        <Pressable onPress={handlePress} style={styles.container}>
            <View style={styles.line}>
                <View>
                    <Text style={styles.title}>SON001</Text>
                    <Text style={styles.text}>Hoai</Text>
                    <Text style={styles.text}>20:27</Text>
                </View>
            </View>
            <View>
                <Text style={styles.text}>10000 VND</Text>
                <Text style={[styles.text, {color: colors.orange}]}>Waiting</Text>
            </View>
        </Pressable>
    )
}

export default OrderComponent;