import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginHorizontal: 5,
        width: windowWidth*0.9,
        padding: 10,
        borderRadius: 10,
        flexDirection: "row",
        borderBottomColor: colors.powder_gray,
        borderBottomWidth: 0.5,
        justifyContent: "space-between",
        alignItems: "center"
    },
    appImage: {
        height: 40, 
        width: 40,
        borderRadius: 40/5,
        backgroundColor: colors.light_purple,
        marginRight: 10
    },
    title: {
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo,
        fontSize: 12
    },
    tinyText: {
        fontSize: 12,
        fontFamily: fonts.UTM_Avo,
    },
    line: {
        flexDirection :"row", alignItems: "center", justifyContent: "space-between"
    }
})

const ProductItem = ({navigation, item}) => {
    const handlePress = () => {
        console.log("Vi Khanh Tun")
        navigation.navigate('DetailProduct')
    }

    return (
        <Pressable onPress={handlePress} style={styles.container}>
            <View style={styles.line}>
                <Image style={styles.appImage} source={require('../assets/images/icon.png')} />
                <View>
                    <Text style={styles.title}>{item.name}</Text>
                    <Text style={styles.text}>{item.des}</Text>
                </View>
            </View>
            <Text style={[styles.title, {fontSize: 14, color: colors.purple}]}>{item.price} VND</Text>
        </Pressable>
    )
}

export default ProductItem;