import React from 'react';
import { View, StyleSheet } from 'react-native';
import { colors } from '../assets/colors/colors';
import { Picker } from '@react-native-picker/picker';

const styles = StyleSheet.create({
    picker: {
        flex: 1,
        marginTop: 10,
        backgroundColor: colors.light_gray,
        borderRadius: 10
    }
})

export default function PickerSelection({selectedValue, onValueChange}) {
    return (
        <View style={styles.picker}>
            <Picker
                selectedValue={selectedValue}
                onValueChange={ (itemValue, itemIndex) =>
                    onValueChange(itemValue)
                }
            >
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js" />
            </Picker>
        </View>
    )
}