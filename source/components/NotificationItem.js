import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    Dimensions
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';

import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.white,
        marginTop: 10,
        marginHorizontal: 5,
        padding: 10,
        width: windowWidth*0.9,
        borderRadius: 10,
        flexDirection: "row",
        borderBottomColor: colors.powder_gray,
        borderBottomWidth: 0.5,
        justifyContent: "space-between"
    },
    title: {
        fontSize: 16,
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo,
        fontSize: 13
    },
    tinyText: {
        fontSize: 12,
        fontFamily: fonts.UTM_Avo,
    },
    line: {
        flexDirection :"row", justifyContent: "space-between", alignItems: "center"
    }
})

const NotificationItem = ({navigation, item}) => {

    const handlePress = () => {
        console.log("Vi Khanh Tun")
        // navigation.navigate('SelectPage')
    }

    const renderIcon = () => {
        switch (item.notifyType) {
            case 1:
                return <Icon name="comments" size={25} color={colors.purple} />
            case 2:
                return <Icon name="inbox" size={25} color={colors.green} />
            case 3:
                return <Icon name="exclamation-triangle" size={25} color={colors.red} />
            case 4:
                return <Icon name="envelope" size={25} color={colors.purple} />
            default:
                return <Icon name="calendar" size={25} color={colors.orange} />
        }
    }

    const renderNameNotify = () => {
        switch (item.notifyType) {
            case 1:
                return colors.purple
            case 2:
                return colors.green
            case 3:
                return colors.red
            case 4:
                return colors.purple
            default:
                return colors.orange
        }
    }

    return (
        <Pressable onPress={handlePress} style={styles.container}>
            <View style={styles.line}>
                {renderIcon()}
                <View style={{paddingLeft: 10}}>
                    <Text style={[styles.title, {color: renderNameNotify()}]}>{item.name}</Text>
                    <Text style={styles.text}>{item.content}</Text>
                </View>
            </View>
            <Text style={styles.tinyText}>{item.timeReceived}</Text>
        </Pressable>
    )
}

export default NotificationItem;