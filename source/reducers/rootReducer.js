import { combineReducers } from 'redux';

import pageReducers from './pageReducers';
import userReducers from './userReducers';
import conservationsReducers from './conservationsReducers';
import messageReducers from './messageReducers';

const rootReducer = combineReducers({
    pageReducers,
    userReducers,
    conservationsReducers,
    messageReducers
});

export default rootReducer;