import {LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT_SUCCESS, LOGOUT_FAIL} from '../actions/actionType'

const initialState = {
    posts: {}
}

export default userReducers = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                code: action.code,
                user: action.user,
                message: action.message
            }
        case LOGIN_FAIL:
            return {
                ...state,
                code: action.code,
                message: action.message
            }
        case LOGOUT_SUCCESS:
            return {
                ...state,
                code: action.code,
                message: action.message
            }
        case LOGOUT_FAIL:
            return {
                ...state,
                code: action.code,
                message: action.message
            }
        default:
            return null;
    }
}