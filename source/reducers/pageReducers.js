import {FETCH_PAGE_SUCCESS, FETCH_PAGE_FAIL} from '../actions/actionType'

const initialState = {
    posts: {}
}

export default PageReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PAGE_SUCCESS:
            return {
                ...state,
                code: action.code,
                pages: action.pages,
                isLoad: action.isLoad,
                message: action.message
            }
        case FETCH_PAGE_FAIL:
            return {
                ...state,
                code: action.code,
                isLoad: action.isLoad,
                message: action.message
            }
        default:
            return null;
    }
}