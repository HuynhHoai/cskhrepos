import {FETCH_CONSERVATIONS_SUCCESS, FETCH_CONSERVATIONS_FAIL} from '../actions/actionType'

const initialState = {
    posts: {}
}

export default ConservationReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_CONSERVATIONS_SUCCESS:
            return {
                ...state,
                code: action.code,
                conservations: action.conservations,
                isLoad: action.isLoad,
                message: action.message
            }
        case FETCH_CONSERVATIONS_FAIL:
            return {
                ...state,
                code: action.code,
                isLoad: action.isLoad,
                message: action.message
            }
        default:
            return null;
    }
}