import {FETCH_MESSAGE_FAIL, FETCH_MESSAGE_SUCCESS} from '../actions/actionType'

const initialState = {
    posts: {}
}

export default MessageReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_MESSAGE_SUCCESS:
            return {
                ...state,
                code: action.code,
                messages: action.messages,
                isLoad: action.isLoad,
                message: action.message
            }
        case FETCH_MESSAGE_FAIL:
            return {
                ...state,
                code: action.code,
                isLoad: action.isLoad,
                message: action.message
            }
        default:
            return null;
    }
}