import { LOGIN, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT, LOGOUT_FAIL, LOGOUT_SUCCESS } from '../actions/actionType';
import { takeLatest, put } from 'redux-saga/effects';
import { API } from '../helpers/API';

function* onLoginUser(action) {
    try {
        const user = yield API.loginUser(action.username, action.password)
        yield console.log(user);
        if (user) {
            yield put ({
                type: LOGIN_SUCCESS,
                code: 1,
                user: user.auth.login,
                message: "OK"
            })
        }
        else {
            yield put({
                type: LOGIN_FAIL,
                code: 0,
                message: "NOT OK"
            })
        }
    } catch (error) {
        console.log(error);
        yield put({
            type: LOGIN_FAIL,
            code: 0,
            message: "NOT OK"
        })
    }
}

export function* watchLoginUSer() {
    yield takeLatest(LOGIN, onLoginUser)
}

function* onLogoutUser(action) {
    try {
        // const user = yield API.loadPost();
        if (true) {
            yield put ({
                type: LOGOUT_SUCCESS,
                code: 1,
                message: "OK"
            })
        }
        else {
            yield put({
                type: LOGOUT_FAIL,
                code: 0,
                message: "NOT OK"
            })
        }
    } catch (error) {
        yield put({
            type: LOGOUT_FAIL,
            code: 0,
            message: "NOT OK"
        })
    }
}

export function* watchLogoutUser() {
    yield takeLatest(LOGOUT, onLogoutUser)
}