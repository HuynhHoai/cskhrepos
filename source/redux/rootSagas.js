import { fork, all } from 'redux-saga/effects';
import { watchFetchPage } from './pageSagas';
import { watchFetchConservation } from './conservationsSagas';
import { watchFetchMessage } from './messageSagas';
import { watchLoginUSer, watchLogoutUser } from './userSagas';

const rootSagas = function*() {
    yield fork(watchFetchPage);
    yield fork(watchLoginUSer);
    yield fork(watchLogoutUser);
    yield fork(watchFetchConservation);
    yield fork(watchFetchMessage);
};

export default rootSagas;