import { FETCH_CONSERVATIONS, FETCH_CONSERVATIONS_SUCCESS, FETCH_CONSERVATIONS_FAIL } from '../actions/actionType';
import { takeLatest, put } from 'redux-saga/effects';
import { API } from '../helpers/API';

function* onFetchConservation(action) {
    try {
        const conservations = yield API.loadConservations();
        if (conservations) {
            yield put ({
                type: FETCH_CONSERVATIONS_SUCCESS,
                code: 1,
                conservations: conservations,
                isLoad: false,
                message: "OK"
            })
        }
        else {
            yield put({
                type: FETCH_CONSERVATIONS_FAIL,
                code: 0,
                isLoad: false,
                message: "NOT OK"
            })
        }
    } catch (error) {
        yield put({
            type: FETCH_CONSERVATIONS_FAIL,
            code: 0,
            isLoad: false,
            message: error.message
        })
    }
}
export function* watchFetchConservation() {
    yield takeLatest(FETCH_CONSERVATIONS, onFetchConservation)
}