import { FETCH_PAGE, FETCH_PAGE_SUCCESS, FETCH_PAGE_FAIL } from '../actions/actionType';
import { takeLatest, put } from 'redux-saga/effects';
import { API } from '../helpers/API';

function* onFetchPage(action) {
    try {
        const pages = yield API.loadPage();
        if (pages) {
            yield put ({
                type: FETCH_PAGE_SUCCESS,
                code: 1,
                pages: pages,
                isLoad: false,
                message: "OK"
            })
        }
        else {
            yield put({
                type: FETCH_PAGE_FAIL,
                code: 0,
                isLoad: false,
                message: "NOT OK"
            })
        }
    } catch (error) {
        yield put({
            type: FETCH_PAGE_FAIL,
            code: 0,
            isLoad: false,
            message: error.message
        })
    }
}
export function* watchFetchPage() {
    yield takeLatest(FETCH_PAGE, onFetchPage)
}