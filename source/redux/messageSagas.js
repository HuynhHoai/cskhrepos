import { FETCH_MESSAGE, FETCH_MESSAGE_FAIL, FETCH_MESSAGE_SUCCESS } from '../actions/actionType';
import { takeLatest, put } from 'redux-saga/effects';
import { API } from '../helpers/API';
import { LOAD_MESSAGE_QUERY } from '../helpers/queries';

function* onFetchMessage(action) {
    try {
        const messages = yield API.fetchData(LOAD_MESSAGE_QUERY, action)
        if (messages) {
            yield put ({
                type: FETCH_MESSAGE_SUCCESS,
                code: 1,
                messages: messages,
                isLoad: false,
                message: "OK"
            })
        }
        else {
            yield put({
                type: FETCH_MESSAGE_FAIL,
                code: 0,
                isLoad: false,
                message: "NOT OK"
            })
        }
    } catch (error) {
        yield put({
            type: FETCH_MESSAGE_FAIL,
            code: 0,
            isLoad: false,
            message: error.message
        })
    }
}
export function* watchFetchMessage() {
    yield takeLatest(FETCH_MESSAGE, onFetchMessage)
}