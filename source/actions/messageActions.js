import { FETCH_MESSAGE } from './actionType'

export const fetchMessages = () => {
    return {
        type: FETCH_MESSAGE
    }
}