import {FETCH_CONSERVATIONS} from './actionType'

export const fetchConservations = () => {
    return {
        type: FETCH_CONSERVATIONS
    }
}