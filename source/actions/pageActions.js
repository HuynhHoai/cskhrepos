import {FETCH_PAGE} from './actionType'

export const fetchPage = () => {
    return {
        type: FETCH_PAGE
    }
}