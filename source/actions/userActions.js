import {LOGIN, LOGOUT} from './actionType'

export const loginUser = (username, password) => {
    return {
        type: LOGIN,
        username,
        password
    }
}

export const logoutSuer = () => {
    return {
        type: LOGOUT
    }
}