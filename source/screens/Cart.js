import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    Pressable,
    Dimensions,
    Image,
    FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import CartItem from '../components/CartItem';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
        backgroundColor: colors.light_gray,
        padding: 10
    },
    header: {
        flex: 1,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10,
        borderRadius: 10
    },
    body: {
        flex: 8,
        marginTop: 10,
        backgroundColor: colors.light_gray,
        alignItems: "center"
    },
    userIcon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    footer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    button: {
        backgroundColor: colors.purple,
        borderRadius: 15,
        width: windowWidth*0.4,
        justifyContent: "space-evenly",
        alignItems: "center",
        height: "75%",
        flexDirection: "row",
    }
})

const DATA = [
    {
        "id": "1",
        "name": "App 1",
        "number": 1,
        "price": 499
    },
    {
        "id": "2",
        "name": "App 2",
        "number": 1,
        "price": 499
    },
    {
        "id": "3",
        "name": "App 3",
        "number": 1,
        "price": 499
    },
    {
        "id": "4",
        "name": "App 4",
        "number": 1,
        "price": 499
    },
    {
        "id": "5",
        "name": "App 1",
        "number": 1,
        "price": 499
    },
    {
        "id": "6",
        "name": "App 2",
        "number": 1,
        "price": 499
    },
    {
        "id": "7",
        "name": "App 3",
        "number": 1,
        "price": 499
    },
    {
        "id": "8",
        "name": "App 4",
        "number": 1,
        "price": 499
    }
]

const Cart = ({navigation}) => {

    const renderItem = ({item, key}) => (
        <CartItem key={key} item={item} navigation={navigation} />
    );

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <Pressable onPress={() => navigation.goBack()}>
                        <Icon name="angle-double-left" size={25} color={colors.black} />
                    </Pressable>
                    <Image style={styles.userIcon} source={require('../assets/images/icon.png')} />
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={DATA}
                    onEndReachedThreshold={.5}
                    ListFooterComponent={()=> <View style={{height: windowHeight*0.1}}/>}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
            <View style={styles.footer}>
                <Pressable style={styles.button}>
                    <Icon name="cart-plus" size={25} color={colors.black} />
                    <Text>Check Out</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default Cart;