import React, { useState, useCallback, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    FlatList,
    Dimensions,
    StatusBar,
    Platform,
    KeyboardAvoidingView
} from 'react-native';
import { colors } from '../assets/colors/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import ImagePicker from "react-native-customized-image-picker";
import { useDispatch, useSelector } from 'react-redux';

import Modal from 'react-native-modal';
import OrderScreen from './OrderScreen';
import InfoOrderScreen from './InfoOrderScreen';
//Chat
import { GiftedChat, Bubble, Send, InputToolbar, Actions, Composer } from 'react-native-gifted-chat';
import { fonts } from '../assets/fonts/font';
import { fetchMessages } from '../actions/messageActions';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        backgroundColor: colors.white,
        padding: 10
    },
    header: {
        marginTop: 20,
        backgroundColor: colors.white,
        justifyContent: "center",
    },
    headerBar: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        justifyContent: "space-between"
    },
    subTitle: {
        fontSize: 18,
        fontFamily: fonts.UTM_AvoBold
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    body: {
        flex: 8,
        marginTop: 10,
        backgroundColor: colors.light_gray,
        alignItems: "center"
    },
    userIcon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    inputToolbar: {
        borderRadius: 25
    },
    circleButton: {
        height: 30, width: 30,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 30/2,
        borderColor: colors.purple,
        borderWidth: 1, marginLeft: 10
    },
})

const MessageScreen = ({navigation, route}) => {

    const {item} = route.params
    
    const dispatch = useDispatch();
    const messageState = useSelector(state => state.messageReducers?.messages);
    const [isLoad, setLoad] = useState(false)

    const [images, setImages] = useState([]);
    const [modalOrderVisible, setModalOrderVisible] = useState(false);
    const [modalInfoVisible, setModalInfoVisible] = useState(false);
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        setLoad(true)
        dispatch(fetchMessages())
    }, [])

    if (messageState != null && isLoad) {
        setLoad(false);
        // let listUsers = []
        // const participants = conservationState.conversation.buckets.map(bucket => bucket.conversations).map(conservations => conservations.map(conservation => conservation.participants));
        // participants.map(participant => participant.map(item => item.map(user => listUsers.push(user))));
        const messageArray = []
        messageState.message.buckets.map(bucket => bucket.messages.map(message => {
            console.log(message.created_time+" - "+Date(message.created_time));
            messageArray.push({
                _id: message._id,
                text: message.message,
                createdAt: message.created_time,
                user: {
                    _id: message.from._id,
                    name: message.from.name,
                    avatar: 'https://placeimg.com/140/140/any',
                },
            })
        }))
        setMessages(messageArray)
    }

    const onSend = useCallback((messages = []) => {
        console.log(messages);
        setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
    }, [])

    const openPicker = async () => {
        try {
            const response = await ImagePicker.openPicker({
                multiple: true,
                isCamera: true
            })
            console.log('done: ', response);
            response.map((item, index) => {
                let message = {
                    _id: messages.length + 1 + index,
                    image: item?.path,
                    createdAt: new Date(),
                    user: {
                        _id: 1,
                        name: 'React Native',
                        avatar: 'https://placeimg.com/140/140/any',
                    },
                }
                onSend([message])
            })
            setImages(response);
        } catch (e) {
            console.log(e);
        }
    };

    const toggleModalOrder = () => {
        setModalOrderVisible(!modalOrderVisible);
    };

    const toggleModalInfo = () => {
        setModalInfoVisible(!modalInfoVisible);
    };

    const renderBubble = (props) => {
        return (
            <Bubble
                {...props}
                wrapperStyle={{
                    right: {
                        backgroundColor: colors.purple
                    },
                    left: {
                        backgroundColor: colors.gray
                    }
                }}
                textStyle={{
                    right: {
                        fontFamily: fonts.UTM_Avo
                    },
                    left: {
                        fontFamily: fonts.UTM_Avo
                    }
                }}
            />
        )
    }

    const renderSend = (props) => {
        return (
            <Send {...props}>
                <View>
                    <MaterialCommunityIcons style={{marginBottom: 5, marginRight: 5}} name="send-circle" size={32} color={colors.purple} />
                </View>
            </Send>
        )
    }

    const renderActions = (props) => {
        return (
          <Actions
            {...props}
            icon={() => (
              <Icon name={"camera"} size={20} color={colors.purple} style={{marginTop: 5, marginLeft: 5}} />
            )}
            onPressActionButton={openPicker}
          />
        );
    }

    const renderInputToolbar = (props) => {
        return (
            <InputToolbar {...props} containerStyle={styles.inputToolbar} />
        )
    }

    const renderComposer = (props) => {
        return (
            <Composer {...props} textInputStyle={{fontFamily: fonts.UTM_Avo}} />
        )
    }

    return (
        <KeyboardAvoidingView
            behavior="height"
            enabled={true}
            style={styles.container}
        >
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <View style={{flexDirection: "row", alignItems: "center"}}>
                        <Pressable style={{paddingRight: 10}} onPress={() => navigation.goBack()}>
                        <Icon name="angle-double-left" size={25} color={colors.black} />
                        </Pressable>
                        <Image style={styles.userIcon} source={require('../assets/images/icon.png')} />
                        <Text numberOfLines={1} style={[styles.subTitle, {maxWidth: windowWidth*0.5}]}>{item.name}</Text>
                    </View>
                    <View style={{flexDirection: "row", alignItems: "center"}}>
                        <Pressable onPress={toggleModalOrder} style={styles.circleButton}>
                            <Icon name="cart-plus" size={15} color={colors.purple} />
                        </Pressable>
                        <Pressable onPress={toggleModalInfo} style={styles.circleButton}>
                            <Icon name="info-circle" size={15} color={colors.purple} />
                        </Pressable>
                    </View>
                </View>
            </View>
            <GiftedChat
                style={{flex: 8}}
                renderBubble={renderBubble}
                renderComposer={renderComposer}
                alwaysShowSend
                renderSend={renderSend}
                renderInputToolbar={renderInputToolbar}
                renderActions={renderActions}
                messages={messages}
                onSend={messages => onSend(messages)}
                user={{
                    _id: "106848268252019",
                    name: 'My Test Page',
                    avatar: 'https://placeimg.com/140/140/any',
                }}
            />
            <Modal
                isVisible={modalOrderVisible}
                style={{margin: 0, justifyContent: "flex-end"}}
            >
                <OrderScreen toggleModal={toggleModalOrder} />
            </Modal>
            <Modal
                isVisible={modalInfoVisible}
                style={{margin: 0, justifyContent: "flex-end"}}
            >
                <InfoOrderScreen toggleModal={toggleModalInfo} />
            </Modal>
        </KeyboardAvoidingView>
    )
}

export default MessageScreen;