import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    Pressable,
    Dimensions,
    Image,
    FlatList
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import CartItem from '../components/CartItem';
import NotificationItem from '../components/NotificationItem';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        padding: 10,
        backgroundColor: colors.white,
    },
    title: {
        fontSize: 22,
        fontFamily: fonts.UTM_AvoBold,
    },
    subTitle: {
        fontSize: 16,
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    header: {
        marginTop: 20,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10,
        borderRadius: 10
    },
    body: {
        marginTop: 10,
        alignItems: "center",
    },
    icon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    image: {  
        height: 60, 
        width: 60,
        marginRight: 10
    },
    card: {
        flexDirection: "row",
        height: 100,
        backgroundColor: colors.light_purple,
        width: windowWidth*0.9,
        justifyContent: "space-between",
        padding: 10,
        alignItems: "center",
        alignSelf: "center",
        borderRadius: 15,
        marginTop: 15,
    },
    miniCard: {
        flexDirection: "row",
        backgroundColor: colors.light_gray,
        width: windowWidth*0.4,
        justifyContent: "space-between",
        padding: 10,
        marginVertical: 15,
        alignItems: "center",
        alignSelf: "center",
        borderRadius: 15,
    },
    line: {
        flexDirection :"row", justifyContent: "space-between"
    }
})
const DATA = [
    {
        "id": "1",
        "name": "New message",
        "content": "Are your going to meet me tonight?",
        "timeReceived": "30 second",
        "notifyType": 1
    },
    {
        "id": "2",
        "name": "New Order Received",
        "content": "You got new order of goods.",
        "timeReceived": "30 second",
        "notifyType": 2
    },
    {
        "id": "3",
        "name": "Server Limit Reached!",
        "content": "Server have 99% CPU usage.",
        "timeReceived": "30 second",
        "notifyType": 3
    },
    {
        "id": "4",
        "name": "New Mail From Peter",
        "content": "Cake sesame snaps cupcake",
        "timeReceived": "30 second",
        "notifyType": 4
    },
    {
        "id": "5",
        "name": "Bruce's Party",
        "content": "Chocolate cake oat cake tiramisu",
        "timeReceived": "30 second",
        "notifyType": 5
    },
    {
        "id": "6",
        "name": "New message",
        "content": "Are your going to meet me tonight?",
        "timeReceived": "30 second",
        "notifyType": 1
    },
    {
        "id": "7",
        "name": "New Order Received",
        "content": "You got new order of goods.",
        "timeReceived": "30 second",
        "notifyType": 2
    },
    {
        "id": "8",
        "name": "Server Limit Reached!",
        "content": "Server have 99% CPU usage.",
        "timeReceived": "30 second",
        "notifyType": 3
    },
    {
        "id": "9",
        "name": "New Mail From Peter",
        "content": "Cake sesame snaps cupcake",
        "timeReceived": "30 second",
        "notifyType": 4
    },
    {
        "id": "10",
        "name": "Bruce's Party",
        "content": "Chocolate cake oat cake tiramisu",
        "timeReceived": "30 second",
        "notifyType": 5
    }
]

const Notification = ({navigation}) => {

    const [checkAll, setCheckAll] = useState(false)
    const renderItem = ({item, key}) => (
        <NotificationItem key={key} item={item} navigation={navigation} />
    );

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    {/* <Pressable onPress={() => navigation.openDrawer()}>
                        <Icon name="bars" size={20} color={colors.powder_gray} />
                    </Pressable> */}
                    <Text style={styles.title}>Notifications</Text>
                    <Pressable>
                        <Text style={{fontFamily: fonts.UTM_AvoBold, color: colors.purple}}>Read all</Text>
                    </Pressable>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={DATA}
                    onEndReachedThreshold={.5}
                    ListFooterComponent={()=> <View style={{height: windowHeight*0.15}}/>}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
        </View>
    )
}

export default Notification;