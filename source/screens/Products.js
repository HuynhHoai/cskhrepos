import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    Pressable,
    Dimensions,
    KeyboardAvoidingView,
    FlatList,
    TextInput
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import Input from '../components/Input';
import ProductItem from '../components/ProductItem';
import Modal from 'react-native-modal';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        padding: 10,
        backgroundColor: colors.white,
    },
    title: {
        fontSize: 22,
        fontFamily: fonts.UTM_AvoBold,
    },
    subTitle: {
        fontSize: 16,
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    header: {
        marginTop: 20,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingBottom: 10,
        borderRadius: 10
    },
    body: {
        flex: 9,
        marginTop: 10,
        alignItems: "center",
    },
    icon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    image: {  
        height: 60, 
        width: 60,
        marginRight: 10
    },
    line: {
        flexDirection :"row", justifyContent: "space-between"
    },
    button: {
        padding: 5,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,
        borderColor: colors.purple,
        borderWidth: 1
    },
    total: {
        flexDirection: "row", justifyContent: "space-between", alignItems: "center", padding: 10, width: windowWidth,
        borderBottomWidth: 1, borderBottomColor: colors.powder_gray
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        backgroundColor: colors.gray,
        height: 35,
        borderRadius: 25,
    },
    modalType: {
        height: windowHeight*0.4, backgroundColor: colors.white, borderTopRightRadius: 15, borderTopLeftRadius: 15, padding: 10
    },
    type: {
        height: 40, borderBottomWidth: 1, justifyContent: 'center', borderColor: colors.dark_gray
    }
})

const DATA = [
    {
        "id": "1",
        "name": "App 1",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "2",
        "name": "App 2",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "3",
        "name": "App 3",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "4",
        "name": "App 4",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "5",
        "name": "App 1",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "6",
        "name": "App 2",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "7",
        "name": "App 3",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "8",
        "name": "App 4",
        "des": "Hoai Dep Trai",
        "price": 499
    }
]

const TYPE_DATA = [
    {
        id: 1,
        type: "All type",
    },
    {
        id: 2,
        type: "Boys",
    },
    {
        id: 3,
        type: "Girls",
    },
    {
        id: 4,
        type: "Male",
    },
    {
        id: 5,
        type: "Female",
    }
]

const Product = ({navigation}) => {

    const [isTypeModal, setTypeModal] = useState(false)

    const renderItem = ({item, key}) => (
        <ProductItem key={key} item={item} navigation={navigation} />
    );

    const renderType = ({key, item}) => (
        <View key={key} style={styles.type}>
            <Text style={styles.text}>{item.type}</Text>
        </View>
    )
    
    const toggleModalType = () => {
        setTypeModal(!isTypeModal)
    }

    return (
        <KeyboardAvoidingView
            behavior="height"
            enabled={false}
            style={styles.container}
        >
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <Text style={styles.title}>Products</Text>
                    <Pressable onPress={() => navigation.navigate('CreateProduct')} style={styles.button}>
                        <Text style={{fontFamily: fonts.UTM_AvoBold, color: colors.purple, fontSize: 13}}>Add new Product</Text>
                    </Pressable>
                </View>
                <View style={styles.inputContainer}>
                    <View style={styles.SectionStyle}>
                        <Icon name="search" size={15} color={colors.purple} />
                        <TextInput
                            style={{flex:1, fontFamily: fonts.UTM_Avo, paddingLeft: 10, fontSize: 13}}
                            placeholder="Enter title..."
                            underlineColorAndroid="transparent"
                        />
                    </View>
                </View>                
            <Pressable onPress={toggleModalType} style={{paddingLeft: 5}}>
                    <Text style={styles.text}>All product type ▼</Text>
                </Pressable>
            </View>
            <View style={styles.body}>
                <View style={styles.total}>
                    <Text style={styles.text}>Total:</Text>
                    <Text style={styles.subTitle}>10</Text>
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={DATA}
                    onEndReachedThreshold={.5}
                    ListFooterComponent={()=> <View style={{height: windowHeight*0.15}}/>}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                />
            </View>

            <Modal
                onBackdropPress={toggleModalType}
                isVisible={isTypeModal}
                style={{margin: 0, justifyContent: "flex-end"}}
            >
                <View style={styles.modalType}>
                    <Text style={{alignSelf: "center", fontFamily: fonts.UTM_AvoBold}}>CHOOSE A TYPE</Text>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={TYPE_DATA}
                        onEndReachedThreshold={.5}
                        ListFooterComponent={()=> <View style={{height: windowHeight*0.1}}/>}
                        renderItem={renderType}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            </Modal>
        </KeyboardAvoidingView>
    )
}

export default Product;