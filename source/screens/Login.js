import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Pressable,
    Dimensions,
    Platform,
    ActivityIndicator,
    Image,
    KeyboardAvoidingView
} from 'react-native'
import CheckBox from '@react-native-community/checkbox'
import { colors } from '../assets/colors/colors';

import {loginUser} from '../actions/userActions';
import {useDispatch, useSelector} from 'react-redux';
import Input from '../components/Input';
import { fonts } from '../assets/fonts/font';
import AsyncStorage from '@react-native-async-storage/async-storage';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

// Set translucent status Bar
StatusBar.setBarStyle("dark-content");
if (Platform.OS === "android") {
    StatusBar.setBackgroundColor("rgba(0,0,0,0)");
    StatusBar.setTranslucent(true);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
        padding: 10
    },
    header: {
        flex: 3,
        marginTop: 20
    },
    body: {
        flex: 7
    },
    title: {
        fontSize: 26,
        fontFamily: fonts.UTM_AvoBold,
        color: colors.purple
    },
    subTitle: {
        fontSize: 20,
        fontFamily: fonts.UTM_Avo
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    checkBoxSection: {
        flexDirection: "row",alignItems: "center"
    },
    row: {
        flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10
    },
    button: {
        backgroundColor: colors.purple,
        padding: 15,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 50,
        borderRadius: 25
    }
})

const Login = ({navigation}) => {

    const userState = useSelector(state => state.userReducers != null ? state.userReducers : null)
    const [isLoad, setLoad] = useState(false);
    const [username, setUsername] = useState('tdtu247');
    const [password, setPassword] = useState('tdtu123456');
    const [toggleCheckBox, setToggleCheckBox] = useState(false);
    const dispatch = useDispatch();

    const handleLogin = () => {
        setLoad(true);
        dispatch(loginUser(username, password));
    }

    const storeAccessToken = async (value) => {
        try {
            await AsyncStorage.setItem('@access_token', value)
        } catch (e) {
            // saving error
        }
    }

    const storeUser = async (value) => {
        try {
            await AsyncStorage.setItem('@user_info', value)
        } catch (e) {
            // saving error
        }
    }

    if (userState != null && isLoad) {
        if (userState.code == 1) {
            const { user } = userState
            storeAccessToken(user.accessToken)
            storeUser(JSON.stringify(user.userData))
        }
        setLoad(false);
    }



    return (
        <KeyboardAvoidingView
            behavior="height"
            enabled={false}
            style={styles.container}
        >
            <View style={styles.header}>
                <Text style={styles.title}>Welcome</Text>
                <Text style={styles.subTitle}>Sign in to continue!</Text>
            <Image source={require('../assets/images/login.png')} resizeMode="contain" style={{height: 150}} />
            </View>
            <View style={styles.body}>
                <Input iconName="user" iconColor={colors.powder_gray} iconSize={20} placeholder="Username" value={username} onChangeText={(text)=> setUsername(text)} />
                <Input iconName="lock" iconColor={colors.powder_gray} iconSize={20} placeholder="Password" value={password} onChangeText={(text)=> setPassword(text)} />
                <View style={styles.row}>
                    <View style={styles.checkBoxSection}>
                        <CheckBox
                            style={styles.checkboxContainer}
                            disabled={false}
                            value={toggleCheckBox}
                            onValueChange={(newValue) => setToggleCheckBox(newValue)}
                        />
                        <Text style={styles.text}>Remember Me!</Text>
                    </View>
                    <Pressable>
                        <Text style={styles.text}>Forgot Password?</Text>
                    </Pressable>
                </View>
                <Pressable onPress={handleLogin} style={styles.button}>
                    {
                        isLoad ? <ActivityIndicator style={{padding: 1}} size="small" color={colors.white} />
                        : <Text style={{color: colors.white, fontFamily: fonts.UTM_Avo, fontSize: 16}}>Login</Text>
                    }
                </Pressable>
                <View style={[styles.row, {justifyContent: "center"}]}>
                    <Text style={styles.text}>New user?</Text>
                    <Pressable onPress={() => navigation.navigate('Register')}>
                        <Text style={{color: colors.purple, fontFamily: fonts.UTM_AvoBold}}> Sign Up</Text>
                    </Pressable>
                </View>
            </View>
        </KeyboardAvoidingView>
    )
}

export default Login