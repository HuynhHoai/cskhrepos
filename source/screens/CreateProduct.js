import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    Pressable,
    StyleSheet,
    ScrollView,
    TextInput,
    StatusBar,
    Image,
    KeyboardAvoidingView
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import {launchImageLibrary} from 'react-native-image-picker'
import { color } from 'react-native-reanimated';
import PickerSelection from '../components/PickerSelection'
import CheckBox from '@react-native-community/checkbox'

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        padding: 10,
        backgroundColor: colors.white,
    },
    header: {
        marginTop: 20,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10,
        borderRadius: 10
    },
    title: {
        flex: 9,
        fontSize: 22,
        fontFamily: fonts.UTM_AvoBold,
    },
    body: {
        flex: 8,
        marginTop: 10,
        marginLeft: 5,
    },
    backScreen: {
        flex: 1,
        flexDirection: "row",
    },
    icon: {
        flex: 1,
    },
    row: {
        marginTop:10,
        flexDirection:"row",
    },
    title_text: {
        marginLeft:10,
        fontSize:18,
        fontFamily: fonts.UTM_AvoBold,
        color:colors.black,
    },
    notice_text: {
        marginLeft: 10,
        fontSize: 13,
        fontFamily: fonts.UTM_Avo,
        color:colors.dark_gray,
    },
    text: { 
        marginTop: 10,
        fontSize: 16,
        color: colors.black,
        fontFamily: fonts.UTM_AvoBold,
    },
    addImage: {
        height: 100,
        width: 100,
        justifyContent:"center",
        alignItems: "center",
        borderStyle: "dotted",
        borderWidth: 2,
        borderRadius:10,
        backgroundColor: colors.light_gray,
        borderColor: colors.dark_gray,
    },
    detail_text: {
        marginTop: 10,
        fontFamily: fonts.UTM_Avo,
        color: colors.black,
    },
    input:{
        marginTop: 10,
        marginRight: 5,
        paddingLeft: 20,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        borderRadius: 10,
        backgroundColor: "#f9f9f9"
    },
    description:{
        marginTop: 10,
        marginRight: 5,
        paddingLeft: 20,
        borderWidth: 1,
        borderColor: colors.dark_gray,
        borderRadius: 10,
        backgroundColor: "#f9f9f9",
        height: 200,
    },
    test:{
        flexDirection:"row",
        justifyContent:"space-between"
    },
    selectPicker:{
        marginTop: 15,
        marginRight: 5,
        borderWidth: 1,
        borderColor:colors.dark_gray,
        borderRadius: 10,
    },
    checkBoxSection: {
        flexDirection: "row",
        alignItems: "center",
        marginTop:15,
        justifyContent:"space-evenly"
    },
    checkBoxEachSection:{
        flexDirection: "row",
    },
    checkboxContainer:{
        marginTop:5
    },
    btnNew:{
        flex:1,
        backgroundColor:colors.gray,
        borderRadius: 10,
        justifyContent:"center",
        alignItems:"center",
        marginRight:5,
        height:40,
    },
    btnSave:{
        flex:1,
        backgroundColor:"#d7a674",
        borderRadius: 10,
        justifyContent:"center",
        alignItems:"center",
        marginRight:5,
    },
    btnTextNew: {
        color:colors.black,
        fontFamily: fonts.UTM_Avo
    },
    btnTextSave: {
        color:colors.white,
        fontFamily: fonts.UTM_Avo
    }
})

const CreateProduct = ({navigation}) =>{
    
    const [image, setImage] = useState({photo:null})
    const handleChoosePhoto = () => {
        const options ={
            noData: true
        }
        launchImageLibrary(options,response =>{
            if(response.uri){
                setImage({photo:response})
            }
        })
    }
    
    const {photo} = image
    const [selectedTrademark, setSelectedTrademark] = useState();
    const [selectedTag, setSelectedTag] = useState();
    const [toggleCheckBoxMale, setToggleCheckBoxMale] = useState()
    const [toggleCheckBoxFeMale, setToggleCheckBoxFeMale] = useState()
    const [toggleCheckBoxBoys, setToggleCheckBoxBoys] = useState()
    const [toggleCheckBoxGirls, setToggleCheckBoxGirls] = useState()

    return (
        <KeyboardAvoidingView
            behavior="height"
            enabled={false}
            style={styles.container}
        >
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <Pressable onPress={() => navigation.goBack()} style={styles.backScreen}>
                        <Icon name="arrow-left" size={20}></Icon>
                    </Pressable>
                    <Text style={styles.title}>Add new product</Text>
                </View>
            </View>
            <View style={styles.body}>
                <ScrollView 
                    showsVerticalScrollIndicator={false}
                >
                    <View style={styles.row}>
                        <View>
                            <Icon name="product-hunt" size={50} color="#d7a674"></Icon>
                        </View>
                        <View>
                            <Text style={styles.title_text}>Add product details</Text>
                            <Text style={styles.notice_text}>Enter your product details here</Text>
                        </View>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.text}>Add product images</Text>
                    </View>
                    <View style={styles.row}>
                        {photo && (
                            <Image
                                source={{uri:photo.uri}}
                                style={{width:100,height:100}}
                            />
                        )}
                        <Pressable style={styles.addImage} onPress={handleChoosePhoto}>
                                <Icon name="plus" size={30} color="#d7a674"></Icon>
                        </Pressable>
                    </View>
                    <View>
                        <Text style={styles.text}>Enter product details</Text>
                        <Text style={styles.detail_text}>Product name</Text>
                        <TextInput
                            style={styles.input}
                        />
                    </View>
                    <View style={styles.test}>
                        <View style={{flex:1}}>
                            <Text style={styles.detail_text}>Price</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={styles.detail_text}>Weight</Text>
                        </View>
                    </View>
                    <View style={styles.test}>
                        <View style={{flex:1}}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                            />
                        </View>
                    </View>
                    <View style={styles.test}>
                        <View style={{flex:1}}>
                            <Text style={styles.detail_text}>Import price</Text>
                        </View>
                        <View style={{flex:1}}>
                            <Text style={styles.detail_text}>Reduced price</Text>
                        </View>
                    </View>
                    <View style={styles.test}>
                        <View style={{flex:1}}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                            />
                        </View>
                        <View style={{flex:1}}>
                            <TextInput
                                style={styles.input}
                                keyboardType={'numeric'}
                            />
                        </View>
                    </View>
                    <View style={styles.selectPicker}>
                    <Picker
                        selectedValue={selectedTrademark}
                        onValueChange={(itemValue, itemIndex) => setSelectedTrademark(itemValue)}
                    >
                        <Picker.Item label="Trademark" value="Trademark" />
                    </Picker>
                    </View>
                    <View style={styles.selectPicker}>
                    <Picker
                        selectedValue={selectedTag}
                        onValueChange={(itemValue, itemIndex) => setSelectedTag(itemValue)}
                    >
                        <Picker.Item label="Tag" value="Tag" />
                    </Picker>
                    </View>
                    <View style={styles.checkBoxSection}>
                        <View style={styles.checkBoxEachSection}>
                            <CheckBox
                                style={styles.checkboxContainer}
                                disabled={false}
                                value={toggleCheckBoxMale}
                                onValueChange={(newValue) => setToggleCheckBoxMale(newValue)}
                            />
                            <Text style={styles.detail_text}>Male</Text>
                        </View>
                        <View style={styles.checkBoxEachSection}>
                            <CheckBox
                                style={styles.checkboxContainer}
                                disabled={false}
                                value={toggleCheckBoxFeMale}
                                onValueChange={(newValue) => setToggleCheckBoxFeMale(newValue)}
                            />
                            <Text style={styles.detail_text}>Female</Text>
                        </View>
                        <View style={styles.checkBoxEachSection}>
                            <CheckBox
                                style={styles.checkboxContainer}
                                disabled={false}
                                value={toggleCheckBoxBoys}
                                onValueChange={(newValue) => setToggleCheckBoxBoys(newValue)}
                            />
                            <Text style={styles.detail_text}>Boys</Text>
                        </View>
                        <View style={styles.checkBoxEachSection}>
                            <CheckBox
                                style={styles.checkboxContainer}
                                disabled={false}
                                value={toggleCheckBoxGirls}
                                onValueChange={(newValue) => setToggleCheckBoxGirls(newValue)}
                            />
                            <Text style={styles.detail_text}>Girls</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={styles.detail_text}>Enter description here</Text>
                        <TextInput
                            style={styles.description}
                            multiline={true}
                        />
                    </View>
                    <View style={{flexDirection:"row",marginTop:15}}>
                        <Pressable
                            style={styles.btnNew}
                        >
                            <Text style={styles.btnTextNew}>New</Text>
                        </Pressable>
                        <Pressable
                            style={styles.btnSave}
                        >
                            <Text style={styles.btnTextSave}>Save</Text>
                        </Pressable>
                    </View>
                    <View style={styles.selectPicker}>
                    <Picker
                        selectedValue={selectedTag}
                        onValueChange={(itemValue, itemIndex) => setSelectedTag(itemValue)}
                    >
                        <Picker.Item label="Tag" value="Tag" />
                    </Picker>
                    </View>
                </ScrollView>
            </View>
        </KeyboardAvoidingView>
    )
}
export default CreateProduct