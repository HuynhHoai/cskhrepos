import React from 'react';
import { View, Text, StyleSheet, StatusBar, Image } from 'react-native';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import {useDispatch} from 'react-redux';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: 10
    },
    header: {
        position: "absolute",
        top: 10,
        width: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    body: {
        flex: 1
    },
    footer: {
        position: "absolute",
        bottom: 10,
        left: 10,
        width: "100%"
    },
    logo: {
        height: 40,
        width: 65
    }
})

const DataDrawerItem = [
    {
        index: 1,
        name: "Tất cả",
        screen: "MessageScreen",
        icon: "inbox",
        params: {}
    },
    {
        index: 2,
        name: "Tin nhắn",
        screen: "MessageScreen",
        icon: "envelope",
        params: {}
    },
    {
        index: 3,
        name: "Bình luận",
        screen: "MessageScreen",
        icon: "comment",
        params: {}
    },
    {
        index: 4,
        name: "Chưa trả lời",
        screen: "MessageScreen",
        icon: "align-justify",
        params: {}
    },
    {
        index: 5,
        name: "Chưa đọc",
        screen: "MessageScreen",
        icon: "eye",
        params: {}
    },
    {
        index: 6,
        name: "Có đơn hàng",
        screen: "MessageScreen",
        icon: "shopping-cart",
        params: {}
    },
    {
        index: 7,
        name: "Có SĐT",
        screen: "MessageScreen",
        icon: "phone-square",
        params: {}
    },
    {
        index: 8,
        name: "Có ghi chú",
        screen: "MessageScreen",
        icon: "sticky-note",
        params: {}
    },
    {
        index: 9,
        name: "Trả lời nhanh",
        screen: "MessageScreen",
        icon: "bolt",
        params: {}
    },
    {
        index: 10,
        name: "Nhãn hội thoại",
        screen: "MessageScreen",
        icon: "microphone",
        params: {}
    }
]

const DrawerContent = (props) => {
    const {state} = props;
    const dispatch = useDispatch();

    let listDrawerItem = DataDrawerItem.map((item, index) => {
        return(
            <DrawerItem
                key={item.index}
                icon={() => (
                    <Icon name={item.icon} size={20} color={colors.powder_gray} />
                )} 
                label={item.name}
                onPress={()=> props.navigation.navigate(item.screen, item.params)}
            />
        )
    })

    const handleLogOut = () => {
        dispatch(loginUser());
    }

    return (
        <View style={styles.container}>
            <DrawerContentScrollView {...props}>
                <View style={styles.header}>
                    <Image source={require('../assets/images/logo.png')} style={styles.logo} />
                </View>
                <View style={styles.body}>
                    {listDrawerItem}

                </View>
            </DrawerContentScrollView>
            <View style={styles.footer}>
                <DrawerItem
                    icon={({color, size}) => (
                        <Icon name="sign-out" size={20} color={colors.powder_gray} />
                    )} 
                    label="Sign Out"
                    onPress={handleLogOut}
                />
            </View>
        </View>
    )
}

export default DrawerContent;