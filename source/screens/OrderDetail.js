import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, StatusBar, Pressable, ImageBackground, Dimensions, FlatList, ScrollView } from 'react-native';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import Icon from 'react-native-vector-icons/FontAwesome';
import ProductItem from '../components/ProductItem';
import Input from '../components/Input';
import Modal from 'react-native-modal';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        backgroundColor: colors.white,
        padding: 10
    },
    header: {
        marginTop: 20,
        backgroundColor: colors.white,
        justifyContent: "center",
    },
    headerBar: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
    },
    subTitle: {
        fontSize: 18,
        fontFamily: fonts.UTM_AvoBold
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    textBold: {
        fontFamily: fonts.UTM_AvoBold
    },
    tinyText: {
        fontFamily: fonts.UTM_Avo,
        fontSize: 11
    },
    body: {
        flex: 8,
        marginTop: 10,
        backgroundColor: colors.light_gray,
        alignItems: "center"
    },
    progress: {
        flexDirection: "row", justifyContent: "space-between", alignItems: "center",
        paddingVertical: 10,
    },
    center: {
        justifyContent: "space-between",
        alignItems: "center"
    },
    line: {
        height: 2, width: 30, backgroundColor: colors.purple,
        marginBottom: 15
    },
    image: {
        resizeMode: "cover", justifyContent: "center", height: 200, width: "100%"
    },
    footer: {
        paddingTop: 10,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    button: {
        flex: 1,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        backgroundColor: colors.purple,
        marginRight: 10
    },
    modalContainer: {
        height: windowHeight*0.3, backgroundColor: colors.white, borderTopRightRadius: 10, borderTopLeftRadius: 10, padding: 20
    },
    modalButton: {
        flexDirection: "row", alignItems: "center", borderBottomWidth: 0.5, paddingVertical: 10
    }
})

const DATA = [
    {
        "id": "1",
        "name": "App 1",
        "des": "Hoai Dep Trai",
        "price": 499
    },
    {
        "id": "2",
        "name": "App 2",
        "des": "Hoai Dep Trai",
        "price": 499
    }
]

const OrderDetail = ({navigation, item}) => {

    const [isTypeModal, setTypeModal] = useState(false)
    const renderProductOrdered = DATA.map((item, index) => {
        return (
            <ProductItem key={index} item={item} navigation={navigation} />
        )
    })

    const toggleModalType = () => {
        setTypeModal(!isTypeModal)
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <Pressable style={{paddingRight: 10}} onPress={() => navigation.goBack()}>
                        <Icon name="angle-double-left" size={25} color={colors.black} />
                    </Pressable>
                    <Text style={styles.subTitle}>SON00001</Text>
                </View>
            </View>

            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.progress}>
                    <View style={styles.center}>
                        <Text style={styles.tinyText}>Waiting</Text>
                        <Icon name="check-circle" size={15} color={colors.purple} />
                        <Text style={styles.tinyText}>20:27</Text>
                        <Text style={styles.tinyText}>2/6/2021</Text>
                    </View>
                    <View style={styles.line} />
                    <View style={styles.center}>
                        <Text style={styles.tinyText}>Waiting</Text>
                        <Icon name="check-circle" size={15} color={colors.purple} />
                        <Text style={styles.tinyText}>20:27</Text>
                        <Text style={styles.tinyText}>2/6/2021</Text>
                    </View>
                    <View style={styles.line} />
                    <View style={styles.center}>
                        <Text style={styles.tinyText}>Waiting</Text>
                        <Icon name="check-circle" size={15} color={colors.purple} />
                        <Text style={styles.tinyText}>20:27</Text>
                        <Text style={styles.tinyText}>2/6/2021</Text>
                    </View>
                    <View style={styles.line} />
                    <View style={styles.center}>
                        <Text style={styles.tinyText}>Waiting</Text>
                        <Icon name="check-circle" size={15} color={colors.purple} />
                        <Text style={styles.tinyText}>20:27</Text>
                        <Text style={styles.tinyText}>2/6/2021</Text>
                    </View>
                    <View style={styles.line} />
                    <View style={styles.center}>
                        <Text style={styles.tinyText}>Waiting</Text>
                        <Icon name="check-circle" size={15} color={colors.purple} />
                        <Text style={styles.tinyText}>20:27</Text>
                        <Text style={styles.tinyText}>2/6/2021</Text>
                    </View>
                </View>
                
                <View style={styles.body}>
                    <ImageBackground opacity={0.5} style={styles.image} source={require('../assets/images/orderBackground.jpg')}>
                        <Text style={styles.subTitle}>0 VND</Text>
                        <Text style={styles.textBold}>Sold by: Shop Hoai</Text>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <Text style={styles.textBold}>Huynh Van Hoai - </Text>
                            <Text style={styles.textBold}>0123456789</Text>
                        </View>
                            <Text style={styles.textBold}>19 Nguyen Huu Tho, Tan Phong, Quan 7, HCM</Text>
                    </ImageBackground>
                    <View style={{marginTop: 10}}>
                        <Text style={[styles.textBold, {paddingBottom: 5}]}>Products: 1</Text>
                        {renderProductOrdered}
                    </View>
                    <View style={{marginTop: 10, width: windowWidth, paddingHorizontal: 10}}>
                        <Text style={[styles.textBold, {paddingBottom: 5}]}>Order Information</Text>
                        <View style={{backgroundColor: colors.white, padding: 10, borderRadius: 10}}>
                            <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                                <Text style={styles.text}>Total purchase goods value</Text>
                                <Text style={styles.text}>0</Text>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                                <Text style={styles.text}>Tax</Text>
                                <Text style={styles.text}>0</Text>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                                <Text style={styles.text}>Discounts</Text>
                                <Text style={styles.text}>0</Text>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                                <Text style={styles.text}>Partner's fee</Text>
                                <Text style={styles.text}>0</Text>
                            </View>
                            <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                                <Text style={styles.textBold}>Customer payment amount</Text>
                                <Text style={styles.text}>0</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{marginTop: 10, width: windowWidth, paddingHorizontal: 10}}>
                        <Text style={[styles.textBold, {paddingBottom: 5}]}>Order note</Text>
                        <Input iconName="comment" iconSize={20} iconColor={colors.purple} placeholder="add note" multiline={true} />
                    </View>
                </View>

            </ScrollView>
            <View style={styles.footer}>
                <Pressable style={styles.button}>
                    <Text style={[styles.text, {color: colors.white}]}>Action</Text>
                </Pressable>
                <Pressable onPress={toggleModalType}>
                    <Icon name="ellipsis-h" size={15} color={colors.purple} />
                </Pressable>
            </View>

            <Modal
                transparent
                onBackdropPress={toggleModalType}
                isVisible={isTypeModal}
                style={{margin: 0, justifyContent: "flex-end"}}
            >
                <View style={styles.modalContainer}>
                    <View style={styles.modalButton}>
                        <Icon name="times-circle" style={{marginRight: 10}} size={15} color={colors.black} />
                        <Text style={styles.text}>Cancel order</Text>
                    </View>
                    <View style={styles.modalButton}>
                        <Icon name="cube" style={{marginRight: 10}} size={15} color={colors.black} />
                        <Text style={styles.text}>Cancel packing</Text>
                    </View>
                    <View style={styles.modalButton}>
                        <Icon name="print" style={{marginRight: 10}} size={15} color={colors.black} />
                        <Text style={styles.text}>Print order</Text>
                    </View>             
                </View>
            </Modal>
        </View>
    )
}

export default OrderDetail;