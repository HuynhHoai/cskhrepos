import React from 'react';
import { View, Text, StyleSheet, Dimensions, Pressable, FlatList } from 'react-native';
import { colors } from '../assets/colors/colors';
import Input from '../components/Input';
import Icon from 'react-native-vector-icons/FontAwesome';
import OrderItem from '../components/OrderItem';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: colors.white
    },
    header: {
        flex: 1,
        borderBottomColor: colors.purple,
        borderBottomWidth: 2,
    },
    body: {
        flex: 9
    },
    button: {
        marginTop: 10,
        marginLeft: 10,
        backgroundColor: colors.purple,
        borderRadius: 15,
        width: windowWidth*0.15,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 15,
        alignSelf: "center"
    },
    title: {
        alignSelf: "flex-start", paddingLeft: 10, fontSize: 16, fontWeight: "bold"
    }
})

const DATA = [
    {
        id: "1",
        createBy: "Test User",
        price: "100000",
        address: "Thành phố Nha Trang, Khánh Hòa",
        phone: "0123456789",
        createAt: "11:16 26/05/2021"
    },
    {
        id: "2",
        createBy: "Test User",
        price: "100000",
        address: "Thành phố Nha Trang, Khánh Hòa",
        phone: "0123456789",
        createAt: "11:16 26/05/2021"
    }
]

const InfoOrderScreen = ({toggleModal}) => {

    const renderOrder = DATA.map((item, index) => {
        return (
            <OrderItem key={index} item={item} />
        )
    })

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Pressable style={{alignSelf: "flex-end"}} onPress={toggleModal}>
                    <Icon name="close" size={25} color={colors.black} />
                </Pressable>
                <Text style={{fontSize: 24, fontWeight: "bold"}}>Thông tin đơn hàng</Text>
            </View>
            <View style={styles.body}>
                <View style={{flexDirection: "row", justifyContent: "center", alignSelf: "center"}}>
                    <Input iconName="sticky-note" iconSize={20} iconColor={colors.powder_gray} multiline={true} placeholder="Tao ghi chu" />
                    <Pressable style={styles.button}>
                        <Icon name="sticky-note" size={20} color={colors.white} />
                    </Pressable>
                </View>
                <View style={{marginTop: 10}}>
                    <Text style={styles.title}>Don hang</Text>
                    {renderOrder}
                </View>
            </View>        
        </View>
    )
}

export default InfoOrderScreen;