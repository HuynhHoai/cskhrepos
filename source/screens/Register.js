import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Pressable,
    Dimensions,
    Platform,
    ActivityIndicator,
    Image
} from 'react-native'
import CheckBox from '@react-native-community/checkbox'
import Icon from 'react-native-vector-icons/FontAwesome'
import { colors } from '../assets/colors/colors';

import {loginUser, RegisterUser} from '../actions/userActions';
import {useDispatch, useSelector} from 'react-redux';
import Input from '../components/Input';
import { fonts } from '../assets/fonts/font';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

// Set translucent status Bar
StatusBar.setBarStyle("dark-content");
if (Platform.OS === "android") {
    StatusBar.setBackgroundColor("rgba(0,0,0,0)");
    StatusBar.setTranslucent(true);
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
        padding: 10
    },
    header: {
        flex: 3,
        marginTop: 20
    },
    body: {
        flex: 7
    },
    title: {
        fontSize: 26,
        fontFamily: fonts.UTM_AvoBold,
        color: colors.black
    },
    subTitle: {
        fontSize: 20,
        fontFamily: fonts.UTM_Avo
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    checkBoxSection: {
        flexDirection: "row",alignItems: "center"
    },
    row: {
        flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 10
    },
    button: {
        backgroundColor: colors.purple,
        padding: 15,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 50,
        borderRadius: 25
    }
})

const Register = ({navigation}) => {

    const userState = useSelector(state => state.userReducers != null ? state.userReducers : null)
    const [isLoad, setLoad] = useState(false);
    const [toggleCheckBox, setToggleCheckBox] = useState(false);
    const dispatch = useDispatch();

    const handleRegister = () => {
        setLoad(true);
        setTimeout(function(){
            dispatch(loginUser());
        }
        ,2000);
    }

    if (userState != null && isLoad) {
        setLoad(false);
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={[styles.title, {color: colors.purple}]}>Create Account</Text>
                <Text style={styles.subTitle}>Sign up to get Started!</Text>
                <Image source={require('../assets/images/login.png')} resizeMode="contain" style={{height: 150}} />
            </View>
            <View style={styles.body}>
                <Input iconName="user" iconColor={colors.powder_gray} iconSize={20} placeholder="Username" />
                <Input iconName="lock" iconColor={colors.powder_gray} iconSize={20} placeholder="Password" />
                <Input iconName="lock" iconColor={colors.powder_gray} iconSize={20} placeholder="Re-Password" />

                <Pressable onPress={handleRegister} style={styles.button}>
                    {
                        isLoad ? <ActivityIndicator style={{padding: 1}} size="small" color={colors.white} />
                        : <Text style={{color: colors.white, fontFamily: fonts.UTM_Avo, fontSize: 16}}>Register</Text>
                    }
                </Pressable>
                <View style={[styles.row, {justifyContent: "center"}]}>
                    <Text style={styles.text}>Already a member?</Text>
                    <Pressable onPress={() => navigation.navigate('LoginScreen')}>
                        <Text style={{color: colors.purple, fontFamily: fonts.UTM_AvoBold}}> Sign In</Text>
                    </Pressable>
                </View>
            </View>
        </View>
    )
}

export default Register