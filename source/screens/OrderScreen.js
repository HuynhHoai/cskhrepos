import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    Pressable,
    StyleSheet,
    ScrollView,
    TextInput,
    Dimensions
} from 'react-native';

import { colors } from '../assets/colors/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import CartItem from '../components/CartItem';
import Input from '../components/Input';
import PickerSelection from '../components/PickerSelection';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 10,
        paddingHorizontal: 10,
        backgroundColor: colors.white
    },
    header: {
        flex: 1,
        borderBottomColor: colors.purple,
        borderBottomWidth: 2,
    },
    body: {
        flex: 9
    },
    inputSection: {
        flex: 1,
        marginTop: 10,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.light_gray,
    },
    input:{
        flex: 1,
        backgroundColor: colors.light_gray,
        borderRadius: 10
    },
    inputIcon: {
        padding: 10
    },
    productList: {
        marginTop: 10,
        borderRadius: 10,
        backgroundColor: colors.light_gray,
        paddingVertical: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    title: {
        alignSelf: "flex-start", paddingLeft: 10, fontSize: 16, fontWeight: "bold"
    },  
    button: {
        backgroundColor: colors.purple,
        borderRadius: 15,
        width: windowWidth*0.35,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 15,
        alignSelf: "center"
    },
    bottomContainer: {
        borderTopWidth: 1, borderTopColor: colors.purple, marginVertical: 10
    },
    bottomOrder: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginTop: 10
    }
})

const DATA = [
    {
        "id": "1",
        "name": "App 1",
        "number": 1,
        "price": 499
    },
    {
        "id": "2",
        "name": "App 2",
        "number": 1,
        "price": 499
    },
    {
        "id": "3",
        "name": "App 3",
        "number": 1,
        "price": 499
    },
    {
        "id": "4",
        "name": "App 4",
        "number": 1,
        "price": 499
    },
    {
        "id": "5",
        "name": "App 1",
        "number": 1,
        "price": 499
    },
    {
        "id": "6",
        "name": "App 2",
        "number": 1,
        "price": 499
    },
    {
        "id": "7",
        "name": "App 3",
        "number": 1,
        "price": 499
    },
    {
        "id": "8",
        "name": "App 4",
        "number": 1,
        "price": 499
    }
]

const OrderScreen = ({toggleModal}) => {

    const [selectedLanguage, setSelectedLanguage] = useState();
    const pickerRef = useRef();

    function open() {
      pickerRef.current.focus();
    }
    
    function close() {
      pickerRef.current.blur();
    }

    const renderProductOrdered = DATA.map((item, index) => {
        return (
            <CartItem key={index} item={item} />
        )
    })

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Pressable style={{alignSelf: "flex-end"}} onPress={toggleModal}>
                    <Icon name="close" size={25} color={colors.black} />
                </Pressable>
                <Text style={{fontSize: 24, fontWeight: "bold"}}>Thông tin đơn hàng</Text>
            </View>
            <View style={styles.body}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <PickerSelection selectedValue={selectedLanguage} onValueChange={setSelectedLanguage} />

                    <Input iconName="phone" iconSize={20} iconColor={colors.powder_gray} placeholder="SDT" />
                    <Input iconName="user" iconSize={20} iconColor={colors.powder_gray} placeholder="Ten nguoi nhan" />

                    <PickerSelection selectedValue={selectedLanguage} onValueChange={setSelectedLanguage} />

                    <Input iconName="cubes" iconSize={20} iconColor={colors.powder_gray} placeholder="Tim san pham" />


                    <View style={styles.productList}>
                        <Text style={styles.title}>Danh sach san pham</Text>
                        {renderProductOrdered}
                    </View>
                    <View style={{marginTop: 10, backgroundColor: colors.white}}>
                        <Text style={styles.title}>Thong tin giao hang</Text>

                        <PickerSelection selectedValue={selectedLanguage} onValueChange={setSelectedLanguage} />

                        <Input iconName="map" iconSize={20} iconColor={colors.powder_gray} placeholder="Address..." />

                        <PickerSelection selectedValue={selectedLanguage} onValueChange={setSelectedLanguage} />
                        <PickerSelection selectedValue={selectedLanguage} onValueChange={setSelectedLanguage} />
                        <PickerSelection selectedValue={selectedLanguage} onValueChange={setSelectedLanguage} />

                    </View>

                    <View style={{marginTop: 10, backgroundColor: colors.white}}>
                        <Text style={styles.title}>Ghi chu</Text>
                        <Input iconName="comment" multiline={true} iconSize={20} iconColor={colors.powder_gray} placeholder="Ghi chu noi bo" />
                        <Input iconName="comment" multiline={true} iconSize={20} iconColor={colors.powder_gray} placeholder="Ghi chu khach" />

                    </View>

                    <View style={{marginTop: 10, backgroundColor: colors.white}}>
                        <Text style={styles.title}>Thong tin thanh toan</Text>

                        <Text style={{marginTop: 10}}>Giam gia</Text>
                        <Input iconName="gift" keyboardType="decimal-pad" multiline={true} iconSize={20} iconColor={colors.powder_gray} placeholder="000.000" />
                        
                        <Text style={{marginTop: 10}}>Chuyen khoan</Text>
                        <Input iconName="bitcoin" keyboardType="decimal-pad" multiline={true} iconSize={20} iconColor={colors.powder_gray} placeholder="000.000" />

                        <Text style={{marginTop: 10}}>Da quet the</Text>
                        <Input iconName="bitcoin" keyboardType="decimal-pad" multiline={true} iconSize={20} iconColor={colors.powder_gray} placeholder="000.000" />

                        <Text style={{marginTop: 10}}>Hinh thuc khac</Text>
                        <Input iconName="bitcoin" keyboardType="decimal-pad" multiline={true} iconSize={20} iconColor={colors.powder_gray} placeholder="000.000" />
                    </View>

                    <View style={styles.bottomContainer}>
                        <View style={styles.bottomOrder}>
                            <Text>Thu ho:</Text>
                            <Text>0 VND</Text>
                        </View>
                        <View style={styles.bottomOrder}>
                            <Pressable style={[styles.button, {backgroundColor: colors.white, borderColor: colors.red, borderWidth: 0.5}]}>
                                <Text style={{color: colors.red, fontWeight: "bold"}}>CANCEL</Text>
                            </Pressable>
                            <Pressable style={styles.button}>
                                <Text style={{color: colors.white, fontWeight: "bold"}}>SAVE</Text>
                            </Pressable>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

export default OrderScreen;