import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    FlatList,
    Dimensions,
    StatusBar,
    ScrollView
} from 'react-native';
import { colors } from '../assets/colors/colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import AppComponent from '../components/AppComponent';
import { fonts } from '../assets/fonts/font';
import Toast from 'react-native-toast-message';
import AsyncStorage from '@react-native-async-storage/async-storage';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        padding: 10,
        backgroundColor: colors.white,
    },
    title: {
        fontSize: 22,
        fontFamily: fonts.UTM_AvoBold,
    },
    subTitle: {
        fontSize: 16,
        fontFamily: fonts.UTM_AvoBold,
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    header: {
        paddingTop: 20, paddingBottom: 10,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10,
        borderRadius: 10
    },
    body: {
        marginTop: 10,
        alignItems: "center",
    },
    icon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    image: {  
        height: 60, 
        width: 60,
        marginRight: 10
    },
    card: {
        flexDirection: "row",
        height: 100,
        backgroundColor: colors.light_purple,
        width: windowWidth*0.9,
        justifyContent: "space-between",
        padding: 10,
        alignItems: "center",
        alignSelf: "center",
        borderRadius: 15,
        marginTop: 15,
    },
    miniCard: {
        flexDirection: "row",
        backgroundColor: colors.light_gray,
        width: windowWidth*0.4,
        justifyContent: "space-between",
        padding: 10,
        marginVertical: 15,
        alignItems: "center",
        alignSelf: "center",
        borderRadius: 15,
    },
    line: {
        flexDirection :"row", justifyContent: "space-between"
    }
})

const DATA = [
    {
        "id": "1",
        "name": "FaceBook"
    },
    {
        "id": "2",
        "name": "Zalo"
    }
]

const DashBoard = ({navigation}) => {

    const [userInfo, setUserInfo] = useState()

    useEffect(()=> {
        async function handleGetData() {
            await getData()
        }
        handleGetData()
        return () => {
            null
        }
    },[]);

    const getData = async () => {
        try {
            const user_info = await AsyncStorage.getItem('@user_info')
            if(user_info !== null) {
                const user = JSON.parse(user_info);
                setUserInfo(user)
                Toast.show({
                    text1: `Welcome back, ${user.display_name}`,
                    topOffset: 50,
                });
            }
        } catch(e) {
            // error reading value
        }
    }

    const renderItem = ({item, key}) => (
        <AppComponent key={key} item={item} navigation={navigation} />
    );

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    {/* <Pressable onPress={() => navigation.openDrawer()}>
                        <Icon name="bars" size={20} color={colors.powder_gray} />
                    </Pressable> */}
                    <Text style={styles.title}>Dashboard</Text>
                    <Image style={styles.icon} source={require('../assets/images/icon.png')} />
                </View>
            </View>
            <View style={styles.body}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.card}>
                        <View style={{flexDirection: "row"}}>
                            <Image source={require('../assets/icons/income.png')} style={styles.image} />
                            <View>
                                <Text style={[styles.text, {color: colors.white}]}>Average Income</Text>
                                <Text style={{fontFamily: fonts.UTM_AvoBold, fontSize: 20, color: colors.white}}>$ 1.000.000</Text>
                            </View>
                        </View>
                        <Icon style={{alignSelf: "flex-start"}} name="ellipsis-h" size={20} color={colors.gray} />
                    </View>
                    <View style={styles.card}>
                        <View style={{flexDirection: "row"}}>
                            <Image source={require('../assets/icons/income.png')} style={styles.image} />
                            <View>
                                <Text style={[styles.text, {color: colors.white}]}>Average Spend</Text>
                                <Text style={{fontFamily: fonts.UTM_AvoBold, fontSize: 20, color: colors.white}}>$ 1.000.000</Text>
                            </View>
                        </View>
                        <Icon style={{alignSelf: "flex-start"}} name="ellipsis-h" size={20} color={colors.gray} />
                    </View>
                    <View style={{marginTop: 40}}>
                        <View style={styles.line}>
                            <Text style={styles.subTitle}>Your Apps</Text>
                            <Icon style={{alignSelf: "flex-start"}} name="ellipsis-h" size={20} color={colors.powder_gray} />
                        </View>
                        <FlatList
                            horizontal
                            data={DATA}
                            onEndReachedThreshold={.5}
                            renderItem={renderItem}
                            keyExtractor={item => item.id.toString()}
                        />
                    </View>
                    <View style={{marginTop: 40}}>
                        <View style={styles.line}>
                            <Text style={styles.subTitle}>Statistics</Text>
                            <Icon style={{alignSelf: "flex-start"}} name="ellipsis-h" size={20} color={colors.powder_gray} />
                        </View>
                        <View style={{flexDirection :"row", justifyContent: "space-around"}}>
                            <Pressable onPress={()=> navigation.navigate("TotalOrders")} style={styles.miniCard}>
                                <View>
                                <Text style={styles.text}>Orders</Text>
                                <Text style={[styles.text, {fontFamily: fonts.UTM_AvoBold}]}>12,345</Text>
                                </View>
                                <Image source={require('../assets/icons/orders.png')} style={styles.icon} />
                            </Pressable>
                            <Pressable onPress={()=> navigation.navigate("TotalOrders")} style={styles.miniCard}>
                                <View>
                                <Text style={styles.text}>Returns</Text>
                                <Text style={[styles.text, {fontFamily: fonts.UTM_AvoBold}]}>12,345</Text>
                                </View>
                                <Image source={require('../assets/icons/returns.png')} style={styles.icon} />
                            </Pressable>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

export default DashBoard;