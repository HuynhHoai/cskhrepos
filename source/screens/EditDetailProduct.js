import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    Pressable,
    StyleSheet,
    ScrollView,
    TextInput,
    StatusBar,
    Dimensions,
    Image,
    Switch,
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import Icon from 'react-native-vector-icons/FontAwesome';
import { color } from 'react-native-reanimated';
import {launchImageLibrary} from 'react-native-image-picker'
import CheckBox from '@react-native-community/checkbox'

const EditDetailProduct = ({navigation}) => {
    const styles = StyleSheet.create({
        row:{
            flexDirection:"row",
            alignItems:"center"
        },
        container: {
            paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
            flex: 1,
            padding: 10,
            backgroundColor: colors.white,
        },
        header: {
            flex: 1,
            backgroundColor: colors.white,
            justifyContent: "center",
            borderRadius: 10
        },
        headerBar: {
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: 10,
            borderRadius: 10
        },
        backScreen: {
            flex:1,
            flexDirection: "row",
        },
        title: {
            flex:4,
            fontSize: 22,
            fontFamily: fonts.UTM_AvoBold,
            justifyContent:"center",
            alignItems:"center"
        },
        body: {
            flex: 8,
            marginTop: 10,
            marginLeft:5,
        },
        imageProduct:{
            height:150,
            width:150,
            borderRadius:5,
        },
        detail_div:{
            marginTop:20,
            backgroundColor:"#e8f0fe",
            borderRadius:5,
            padding:10,
        },
        notice_detail:{
            fontSize:20,
            color:colors.dark_gray,
        },
        text:{
            fontSize:19,
            color:colors.black
        },
        hr:{
            marginTop:10,
            marginBottom:10,
            borderBottomWidth:0.7,
            borderBottomColor:colors.dark_gray,
        },
        btn_save_div:{
            marginTop:20,
            justifyContent:"center",
            alignItems:"center",
        },
        btn_save:{
            width:150,
            borderWidth:1,
            borderRadius:5,
            borderColor:colors.green,
            padding:10,
            backgroundColor:colors.green,
            alignItems:"center"
        },
        btn_save_text:{
            fontSize:16,
            color:colors.white,
        },
        addImage:{
            height:150,
            width:150,
            justifyContent:"center",
            alignItems:"center",
            borderStyle:"dotted",
            borderWidth:2,
            borderStyle:"dotted",
            borderRadius:5,
            backgroundColor:"#f9f9f9",
            borderColor:colors.dark_gray,
        },
        input:{
            borderBottomWidth:1,
            borderBottomColor:colors.dark_gray,
            paddingLeft:15,
            marginBottom:10,
            fontSize:19,
            marginRight:10,
        },
        picker:{
            borderWidth:1,
            borderColor:colors.dark_gray,
            borderRadius:5,
            marginTop:10,
            marginBottom:10,
        },
        checkBoxSection: {
            flexDirection: "row",
            alignItems: "center",
            marginTop:15,
            justifyContent:"space-evenly"
        },
        checkBoxEachSection:{
            flexDirection: "row",
        },
        checkboxContainer:{
            marginTop:5
        },
        detail_text:{
            marginTop:10,
            fontSize:16,
            color: colors.dark_gray,
        },
    })
    const [image,setImage] = useState({photo:null})
    const handleChoosePhoto = ()=>{
        const options ={
            noData: true
        }
        launchImageLibrary(options,response =>{
            if(response.uri){
                setImage({photo:response})
            }
        })
    }
    const {photo} = image
    const [selectedTrademark, setSelectedTrademark] = useState();
    const [selectedTag, setSelectedTag] = useState();
    const [productName,setProductName] = useState();
    const [toggleCheckBoxMale,setToggleCheckBoxMale] = useState()
    const [toggleCheckBoxFeMale,setToggleCheckBoxFeMale] = useState()
    const [toggleCheckBoxBoys,setToggleCheckBoxBoys] = useState()
    const [toggleCheckBoxGirls,setToggleCheckBoxGirls] = useState()
    const [isEnabledTax, setIsEnabledTax] = useState(false);
    const toggleSwitchTax = () => setIsEnabledTax(previousState => !previousState);
    const [isEnabledSale, setIsEnabledSale] = useState(false);
    const toggleSwitchSale = () => setIsEnabledSale(previousState => !previousState);
    return(
        <View style={styles.container}>
             <View style={styles.header}>
                <View style={styles.headerBar}>
                    <Pressable style={styles.backScreen}>
                        <Icon name="times" size={20}></Icon>
                    </Pressable>
                    <Text style={styles.title}>Giày Fake's Details</Text>
                    <Pressable>
                        <Icon name="check" size={20}></Icon>
                    </Pressable>
                </View>
            </View>
            <View style={styles.body}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.row}>
                        {photo && (
                            <Image
                                source={{uri:photo.uri}}
                                style={{width:150,height:150}}
                            />
                        )}
                        <Pressable style={styles.addImage} onPress={handleChoosePhoto}>
                                <Icon name="plus" size={30} color={colors.green}></Icon>
                        </Pressable>
                    </View>
                    <View style={styles.detail_div}>
                        <View>
                            <Text style={styles.notice_detail}>Product name</Text>
                            <TextInput style={styles.input}></TextInput>
                        </View>
                        <View>
                            <Text style={styles.notice_detail}>Weight(g)</Text>
                            <TextInput style={styles.input}></TextInput>
                        </View>
                        <View>
                            <Text style={styles.notice_detail}>Trademark</Text>
                            <View style={styles.picker}> 
                                <Picker
                                    selectedValue={selectedTrademark}
                                    onValueChange={(itemValue, itemIndex) => setSelectedTrademark(itemValue)}
                                >
                                    <Picker.Item label="Chọn thương hiệu" value="..." />
                                </Picker>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.notice_detail}>Tags</Text>
                            <View style={styles.picker}>
                                <Picker
                                    selectedValue={selectedTag}
                                    onValueChange={(itemValue, itemIndex) => setSelectedTag(itemValue)}
                                >
                                    <Picker.Item label="Chọn Tag" value="..." />
                                </Picker>
                            </View>
                        </View>
                        <View>
                            <Text style={styles.notice_detail}>Size</Text>
                            <TextInput style={styles.input}></TextInput>
                        </View>
                        <View>
                            <Text style={styles.notice_detail}>Gender</Text>
                            <View style={styles.checkBoxSection}>
                                <View style={styles.checkBoxEachSection}>
                                    <CheckBox
                                        style={styles.checkboxContainer}
                                        disabled={false}
                                        value={toggleCheckBoxMale}
                                        onValueChange={(newValue) => setToggleCheckBoxMale(newValue)}
                                    />
                                    <Text style={styles.detail_text}>Male</Text>
                                </View>
                                <View style={styles.checkBoxEachSection}>
                                    <CheckBox
                                        style={styles.checkboxContainer}
                                        disabled={false}
                                        value={toggleCheckBoxFeMale}
                                        onValueChange={(newValue) => setToggleCheckBoxFeMale(newValue)}
                                    />
                                    <Text style={styles.detail_text}>Female</Text>
                                </View>
                                <View style={styles.checkBoxEachSection}>
                                    <CheckBox
                                        style={styles.checkboxContainer}
                                        disabled={false}
                                        value={toggleCheckBoxBoys}
                                        onValueChange={(newValue) => setToggleCheckBoxBoys(newValue)}
                                    />
                                    <Text style={styles.detail_text}>Boys</Text>
                                </View>
                                <View style={styles.checkBoxEachSection}>
                                    <CheckBox
                                        style={styles.checkboxContainer}
                                        disabled={false}
                                        value={toggleCheckBoxGirls}
                                        onValueChange={(newValue) => setToggleCheckBoxGirls(newValue)}
                                    />
                                    <Text style={styles.detail_text}>Girls</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    <View style={styles.detail_div}>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Purchase price</Text>
                                <TextInput style={styles.input}></TextInput>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Reduced price</Text>
                                <TextInput style={styles.input}></TextInput>
                            </View> 
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Icon name="check-circle" size={30} color={colors.green} style={{marginRight:10}}></Icon>
                            </View>
                            <View style={{flex:8}}>
                                <Text style={styles.text}>Tax applied</Text>
                            </View>
                            <View>
                            <Switch
                                trackColor={{ false: "#767577", true: "#81b0ff" }}
                                thumbColor={isEnabledTax ? "#f5dd4b" : "#f4f3f4"}
                                onValueChange={toggleSwitchTax}
                                value={isEnabledTax}
                            />
                            </View>
                        </View>
                    </View>
                    <View style={styles.detail_div}>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Icon name="check-circle" size={30} color={colors.green} style={{marginRight:10}}></Icon>
                            </View>
                            <View style={{flex:8}}>
                                <Text style={styles.text}>Available for sales</Text>
                            </View>
                            <View>
                            <Switch
                                trackColor={{ false: "#767577", true: "#81b0ff" }}
                                thumbColor={isEnabledSale ? "#f5dd4b" : "#f4f3f4"}
                                onValueChange={toggleSwitchSale}
                                value={isEnabledSale}
                            />
                            </View>
                        </View>
                    </View>
                    <View style={styles.btn_save_div}>
                        <Pressable style={styles.btn_save}>
                            <Text style={styles.btn_save_text}>Save</Text>
                        </Pressable>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

export default EditDetailProduct