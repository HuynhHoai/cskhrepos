import React, { useRef, useState } from 'react';
import {
    View,
    Text,
    Pressable,
    StyleSheet,
    ScrollView,
    TextInput,
    StatusBar,
    Dimensions,
    Image,
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({
    row:{
        flexDirection:"row",
        alignItems:"center"
    },
    container: {
        paddingTop: StatusBar.currentHeight, // Set this to avoid Status Bar
        flex: 1,
        padding: 10,
        backgroundColor: colors.white,
    },
    header: {
        marginTop: 20,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingHorizontal: 10,
        borderRadius: 10
    },
    backScreen: {
        flex:1,
        flexDirection: "row",
    },
    title: {
        flex:9,
        fontSize: 22,
        fontFamily: fonts.UTM_AvoBold,
    },
    body: {
        flex: 8,
        marginTop: 10,
        marginLeft:5,
    },
    imageProduct:{
        height: 100,
        width: 100,
        borderRadius: 5,
        marginRight: 20,
    },
    detail_div:{
        marginTop: 20,
        backgroundColor:"#e8f0fe",
        borderRadius: 5,
        padding: 10,
    },
    detail_title:{
        fontSize: 18,
        color: colors.black,
        fontFamily: fonts.UTM_AvoBold
    },
    notice_detail:{
        fontSize: 16,
        color: colors.dark_gray,
        fontFamily: fonts.UTM_Avo
    },
    text:{
        color: colors.black,
        fontFamily: fonts.UTM_Avo
    },
    hr:{
        marginTop: 10,
        marginBottom: 10,
        borderBottomWidth: 0.7,
        borderBottomColor: colors.dark_gray,
    },
    btn_delete_div:{
        marginTop: 20,
        justifyContent:"center",
        alignItems:"center",
    },
    btn_delete:{
        borderRadius: 10,
        borderColor: colors.red,
        padding: 10,
        backgroundColor: "#d32121",
    },
    btn_delete_text:{
        fontSize: 16,
        color: colors.white,
        fontFamily: fonts.UTM_Avo

    }
})

const DetailProduct = ({navigation}) => {
    
    return(
        <View style={styles.container}>
             <View style={styles.header}>
                <View style={styles.headerBar}>
                    <Pressable onPress={()=> navigation.goBack()} style={styles.backScreen}>
                        <Icon name="arrow-left" size={20}></Icon>
                    </Pressable>
                    <Text style={styles.title}>Product Details</Text>
                    <Pressable onPress={()=> navigation.navigate("EditDetailProduct")}>
                        <Icon name="pencil" size={20}></Icon>
                    </Pressable>
                </View>
            </View>
            <View style={styles.body}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                    >
                        <Image style={styles.imageProduct} source={require('../assets/images/login.png')}/>
                        <Image style={styles.imageProduct} source={require('../assets/images/login.png')}/>
                        <Image style={styles.imageProduct} source={require('../assets/images/login.png')}/>
                        <Image style={styles.imageProduct} source={require('../assets/images/login.png')}/>
                    </ScrollView>
                    <View style={styles.detail_div}>
                        <Text style={styles.detail_title}>Giày fake</Text>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Weight</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>: Test</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Trademark</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>: Test</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Tags</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>: Test</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Gender</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>: Test</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Size</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>: Test</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.detail_div}>
                        <View style={styles.row}>
                            <View style={{flex:7}}>
                                <Text style={styles.detail_title}>In stock</Text>
                            </View>
                            <View style={{flex:2}}>
                                <Text style={styles.notice_detail}>In stock: </Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={{color:colors.red,fontSize:19}}>100</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:9}}>
                                <Text style={styles.notice_detail}>Storage location:---</Text>
                            </View>
                        </View>
                        <View style={styles.hr}></View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Purchase price</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.notice_detail}>Reduced price</Text>
                            </View>
                        </View>
                        <View style={styles.row}>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>$ 250</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Text style={styles.text}>$ 50</Text>
                            </View>
                        </View>
                        <View style={styles.hr}></View>
                        <View style={styles.row}>
                            <Icon name="check-circle" size={30} color={colors.green} style={{marginRight:10}}></Icon>
                            <Text style={styles.text}>Tax applied</Text>
                        </View>
                    </View>
                    <View style={styles.detail_div}>
                        <View style={styles.row}>
                            <Icon name="check-circle" size={30} color={colors.green} style={{marginRight:10}}></Icon>
                            <Text style={styles.text}>Available for sales</Text>
                        </View>
                    </View>
                    <View style={styles.btn_delete_div}>
                        <Pressable style={styles.btn_delete}>
                            <Text style={styles.btn_delete_text}>Delete product</Text>
                        </Pressable>
                    </View>
                </ScrollView>
            </View>
        </View>
    )
}

export default DetailProduct