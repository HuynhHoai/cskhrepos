import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Pressable,
    Image,
    FlatList,
    Dimensions,
    StatusBar,
    Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import PageComponent from '../components/PageComponent';
import {fetchPage} from '../actions/pageActions';
import { useDispatch, useSelector } from 'react-redux';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight,
        backgroundColor: colors.white,
        flex: 1,
        padding: 10
    },
    header: {
        flex: 1,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        justifyContent: "space-between"
    },
    subTitle: {
        fontSize: 20,
        fontFamily: fonts.UTM_AvoBold
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    body: {
        flex: 8,
        marginTop: 10,
        backgroundColor: colors.white,
        alignItems: "center"
    },
    messageIcon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    footer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingTop: 10,
    },
    button: {
        flex: 1,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        backgroundColor: colors.purple,
        marginRight: 10,
    },
    circleButton: {
        height: 30, width: 30,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 30/2,
        borderColor: colors.purple,
        borderWidth: 1, marginLeft: 10
    },
    circleButtonActive: {
        height: 30, width: 30,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 30/2,
        backgroundColor: colors.purple,
        marginLeft: 10
    },
})


const SelectPage = ({navigation}) => {
    const dispatch = useDispatch();
    const pagesState = useSelector(state => state.pageReducers?.pages);
    const [pages, setPages] = useState([])
    const [isLoad, setLoad] = useState(false)
    useEffect(()=> {
        async function AsyncGetData() {
            setLoad(true)
            dispatch(fetchPage())
        }
        AsyncGetData();
        return () => {
            null
        }
    },[]);

    if(pagesState != null && isLoad) {
        setLoad(false);
        setPages(pagesState.page.pages)
    }

    const [checkAll, setCheckAll] = useState(false)

    const renderItem = ({item, key}) => (
        <PageComponent key={key} item={item} handleCheckPage={() => handleCheckPage(item, key)} />
    );

    const handleCheck = () => {
        setCheckAll(!checkAll)
        const newData = pages.map(item => {
            return {
                ...item,
                selected: !checkAll
            }
        })
        setPages(newData)
    }

    const handleCheckPage = (itemSelected, key) => {
        const newData = pages.map(item => {
            if (itemSelected._id == item._id) {
                return {
                    ...item,
                    selected: !item.selected
                }
            }
            return {
                ...item,
                selected: item.selected
            }
        })
        setPages(newData);
    }

    const handleComeIn = () => {
        const listSelected = pages.filter(item => item.selected == true)
        let pagesIDs = listSelected.map(item => {
            return { _id: item._id }
        })
        console.log(pagesIDs);
        navigation.navigate("HomeScreen", {pagesIDs})
    }

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <View style={{flexDirection: "row", alignItems: "center"}}>
                        <Pressable style={{paddingRight: 10}} onPress={() => navigation.goBack()}>
                        <Icon name="angle-double-left" size={25} color={colors.black} />
                        </Pressable>
                        <Text style={styles.subTitle}>Pages</Text>
                    </View>
                    <View style={{flexDirection: "row", alignItems: "center"}}>
                        <Pressable onPress={handleCheck} style={checkAll ? styles.circleButtonActive : styles.circleButton}>
                            <Icon name="check" size={15} color={checkAll ? colors.white : colors.purple} />
                        </Pressable>
                        <Pressable style={styles.circleButton}>
                            <Icon name="plus" size={15} color={colors.purple} />
                        </Pressable>
                    </View>
                </View>
            </View>
            <View style={styles.body}>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={pages}
                    onEndReachedThreshold={.5}
                    renderItem={renderItem}
                    keyExtractor={item => item._id.toString()}
                />
            </View>
            <View style={styles.footer}>
                <Pressable style={styles.button} onPress={handleComeIn}>
                    <Text style={[styles.text, {color: colors.white}]}>Get In</Text>
                </Pressable>
                <Pressable>
                    <Image style={styles.messageIcon} source={require('../assets/icons/facebook.png')}/>
                </Pressable>
            </View>
        </View>
    )
}

export default SelectPage;