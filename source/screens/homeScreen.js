import React, { useEffect, useState, useRef } from 'react';
import {
    View,
    Text,
    StyleSheet,
    StatusBar,
    FlatList,
    Pressable,
    Image,
    Dimensions,
    Animated,
    TextInput,
    KeyboardAvoidingView,
    Easing
} from 'react-native';

import MessageItem from "../components/MessageItem";
import Icon from 'react-native-vector-icons/FontAwesome';

import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../assets/colors/colors';
import { fonts } from '../assets/fonts/font';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { fetchConservations } from '../actions/conservationsActions';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        paddingTop: StatusBar.currentHeight,
        backgroundColor: colors.white,
        flex: 1,
        padding: 10
    },
    header: {
        paddingTop: 20,
        paddingBottom: 10,
        backgroundColor: colors.white,
        justifyContent: "center",
        borderRadius: 10
    },
    headerBar: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        justifyContent: "space-between"
    },
    subTitle: {
        fontSize: 20,
        fontFamily: fonts.UTM_AvoBold
    },
    text: {
        fontFamily: fonts.UTM_Avo
    },
    body: {
        marginTop: 10,
        backgroundColor: colors.white,
        alignItems: "center"
    },
    messageIcon: {
        height: 40, 
        width: 40,
        borderRadius: 40/2
    },
    footer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    button: {
        flex: 1,
        padding: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 25,
        backgroundColor: colors.purple,
        marginRight: 10
    },
    circleButton: {
        height: 30, width: 30,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 30/2,
        borderColor: colors.purple,
        borderWidth: 1, marginLeft: 10
    },
    circleButtonActive: {
        height: 30, width: 30,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 30/2,
        backgroundColor: colors.purple,
        marginLeft: 10
    },
    tabBarButton: {
        backgroundColor: colors.purple, padding: 7, borderRadius: 20
    },
    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 10,
        backgroundColor: colors.gray,
        height: 35,
        borderRadius: 25,
    },
})

const homeScreen = ({navigation, route}) => {

    const { pagesIDs } = route.params;
    const dispatch = useDispatch();
    const conservationState = useSelector(state => state.conservationsReducers?.conservations);
    const [conservations, setConservation] = useState([])
    const [isLoad, setLoad] = useState(false)


    useEffect(()=> {
        async function AsyncGetData() {
            setLoad(true)
            dispatch(fetchConservations())
        }
        AsyncGetData();
        return () => {
            null
        }
    },[]);

    if (conservationState != null && isLoad) {
        setLoad(false);
        let listUsers = []
        const participants = conservationState.conversation.buckets.map(bucket => bucket.conversations).map(conservations => conservations.map(conservation => conservation.participants));
        conservationState.conversation.buckets.map(bucket => bucket.conversations).map(conservations => conservations.map(conservation => console.log(conservation)));
        participants.map(participant => participant.map(item => item.map(user => listUsers.push(user))));
        setConservation(listUsers)
    }

    const renderListMessage = ({item}) => {
        return (
            <MessageItem key={item._id} navigation={navigation} item={item} />
        )
    };

    const FirstRoute = () => (
        <View style={{ flex: 1}}>
            <FlatList
                data={conservations}
                showsVerticalScrollIndicator={false}
                renderItem={item => renderListMessage(item)}
                onEndReachedThreshold={.5}
                ListFooterComponent={()=> <View style={{height: windowHeight*0.1}}/>}
                keyExtractor={item => item._id}
            />
        </View>
    );
    
    const SecondRoute = () => (
        <View style={{ flex: 1}}>
            <Text>No information</Text>
            {/* <FlatList
                data={conservations}
                showsVerticalScrollIndicator={false}
                renderItem={item => renderListMessage(item)}
                onEndReachedThreshold={.5}
                ListFooterComponent={()=> <View style={{height: windowHeight*0.1}}/>}
                keyExtractor={item => item.id}
            /> */}
        </View>
    );
    
    const renderScene = SceneMap({
        messages: FirstRoute,
        comments: SecondRoute,
    });

    const [index, setIndex] = useState(0);
    const [routes] = React.useState([
        { key: 'messages', title: 'Messages' },
        { key: 'comments', title: 'Comments' },
    ]);

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorStyle={{ backgroundColor: colors.white }}
            style={{ backgroundColor: colors.white }}
            renderLabel={({ route, focused, color }) => (
                <View style={[styles.tabBarButton, {backgroundColor: focused ? colors.purple : colors.white}]}>
                    <Text style={{color: focused ? colors.white : colors.black, fontFamily: fonts.UTM_AvoBold, fontSize: 12}}>
                        {route.title}
                    </Text>
                </View>
              )}
        />
    );
    
    return (
        <KeyboardAvoidingView
            behavior="height"
            enabled={false}
            style={styles.container}
        >
            <View style={styles.header}>
                <View style={styles.headerBar}>
                    <View style={{flexDirection: "row", alignItems: "center"}}>
                        <Pressable style={{paddingRight: 10}} onPress={() => navigation.goBack()}>
                        <Icon name="angle-double-left" size={25} color={colors.black} />
                        </Pressable>
                        <Text style={styles.subTitle}>Chat</Text>
                    </View>

                    <Pressable style={styles.circleButton}>
                        <Icon name="filter" size={15} color={colors.purple} />
                    </Pressable>
                </View>
            </View>

            <View style={styles.inputContainer}>
                <View style={styles.SectionStyle}>
                    <Icon name="search" size={15} color={colors.purple} />
                    <TextInput
                        style={{flex:1, fontFamily: fonts.UTM_Avo, paddingLeft: 10, fontSize: 13}}
                        placeholder="Enter title..."
                        underlineColorAndroid="transparent"
                    />
                </View>
            </View>

            <TabView
                style={{flex: 9, padding: 0}}
                renderTabBar={renderTabBar}
                navigationState={{ index, routes }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                initialLayout={{ width: windowWidth }}
                lazy
            />
        </KeyboardAvoidingView>
        
    )
}


export default homeScreen;