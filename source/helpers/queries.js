import {gql} from "@apollo/client";

export const LOGIN_USER_QUERY = gql`
    query AuthQuery($username: String!, $password: String!) {
        auth {
            login (username: $username, password: $password) {
                accessToken
                userData{
                    email
                    display_name
                }
            }
        }
    }
`

export const LOAD_PAGE_QUERY = gql`
    query PageQuery {
        page {
            pages {
                _id
                name
                category
                category_list {
                    _id
                    name
                }
                page_intro
                is_active
            }
        }
    }
`

export const LOAD_CONSERVATION_QUERY = gql `
    query {
        conversation {
            buckets {
                _id
                page_id
                day
                count
                conversations {
                    page_id
                    _id
                    updated_time
                    is_read
                    post_id
                    has_note
                    has_order
                    phone_numbers
                    is_replied
                    participants {
                    _id
                    name
                    }
                }
            }
        }
    }
`

export const LOAD_MESSAGE_QUERY = gql `
    query {
        message {
            buckets {
                _id
                conversation_id
                day
                count
                messages {
                    created_time
                    _id
                    message
                    from {
                        _id
                        name
                    }
                }
            }
        }
    }
`