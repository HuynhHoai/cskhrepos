import axios from "axios";
import { ApolloClient, InMemoryCache, HttpLink, ApolloLink } from '@apollo/client'
import { LOAD_CONSERVATION_QUERY, LOAD_PAGE_QUERY, LOGIN_USER_QUERY } from "./queries";
import AsyncStorage from "@react-native-async-storage/async-storage";

let access_token = ''

const httpLink = new HttpLink({ uri: 'https://chat-service-dev.azsales.vn/graphql' });
const authLink = new ApolloLink( async (operation, forward) => {

    access_token = await AsyncStorage.getItem('@access_token')
    operation.setContext({
        headers: {
            "access_Token": access_token
        }
    });
    // Call the next link in the middleware chain.
    return forward(operation);
});

const client = new ApolloClient({
    uri: 'https://auth-service-dev.azsales.vn/graphql',
    cache: new InMemoryCache()
});

const chat_client = new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache()
})

function* loginUser(username, password) {
    console.log(username, password);
    let temp = null;
    yield client.query({
        query: LOGIN_USER_QUERY,
        variables: { username, password }
    })
    .then(result => temp = result.data);
    const result = yield temp != null ? temp : null
    return result;
}

function* loadPage() {
    let temp = null;
    yield chat_client.query({
        query: LOAD_PAGE_QUERY,
    })
    .then(result => temp = result.data);
    const result = yield temp != null ? temp : null
    return result;
}

function* loadConservations() {
    let temp = null;
    yield chat_client.query({
        query: LOAD_CONSERVATION_QUERY,
    })
    .then(result => temp = result.data);
    const result = yield temp != null ? temp : null
    return result;
}

function* fetchData(QUERY, action) {
    let temp = null;
    yield chat_client.query({
        query: QUERY,
    })
    .then(result => temp = result.data);
    const result = yield temp != null ? temp : null
    return result;
}

export const API = {
    loadPage,
    loginUser,
    loadConservations,
    fetchData
}